#!/bin/bash
dir=`pwd`
dockername=$1
echo Start docker with base dir: $dir
echo Init supervisor log at $dir/logs/$dockername
mkdir -p $dir/logs/$dockername
dockerid=`docker run -d \
    --name $dockername \
    -v $dir/runtime:/data \
    -v $dir/supervisor/$dockername:/var/log/supervisor \
    cookier/rserver`
ip=`docker inspect $dockerid|grep IPAddress|cut -d '"' -f4`
echo $dockername running with IP:$ip
