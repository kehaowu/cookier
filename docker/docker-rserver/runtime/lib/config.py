import socket
docker=False
port=60010          #R manager port
host="localhost"    #R manager host, should be changed with the docker ip address
clientport=60011    #R client port
clienthost=socket.gethostname()      #R client host
portinit=60020      #R process port pool from 60020 to 60019
portn=100   #Number of R process in pool
redisport=6379
redishost="localhost"
