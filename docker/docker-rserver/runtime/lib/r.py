import signal,redis,json,time
from os import kill as prockill
from os.path import exists as pathexists

class base():
    def __init__(self,server=False,\
            port=60010,host="localhost",\
            clientport=60011,clienthost="localhost",\
            portinit=60020,portn=100,redisport=6379,\
            redishost='localhost'):
        print "Here is rbase"
        self.portinit = portinit + 1
        self.port = port
        self.host = host
        self.clientport = clientport
        self.clienthost = clienthost
        self.redis = redis.Redis(host=redishost,port=redisport,db=0)
        self.portset = range(self.portinit,self.portinit+portn)
        self.portmin = min(self.portset)
        self.portmax = min(self.portset)
        if server :
            self.flushall()
        for port in self.portset:
            self.redis.sadd("dead:port",port)
    def flushall(self):
        print "Empty redis data."
        self.redis.flushall()
    def checkport(self,port=None):
        if port == None:
            print "Check any port available or not."
            if self.redis.scard("dead:port") == 0:
                return False
            else:
                return True
        elif port <= self.portmax and port >= self.portmin:
            print "Check if port",port,"available or not."
            return self.redis.sismember("dead:port",port)
        else:
            print "Port",port,"is illegal port:",self.portmin,"to",self.portmax
            return False
    def randomport(self):
        if self.checkport():
            return self.redis.srandmember("dead:port")
        else:
            return False
    def rprocstart(self,token,port,pid,expire=600):
        # Move port to alive:port set
        self.redis.smove("dead:port","alive:port",port)
        # Set token:pid in hash table
        self.redis.hset("token:pid",token,pid)
        # Set token:port in hash table
        self.redis.hset("token:port",token,port)
        # Add pid to set alive:pid
        self.redis.sadd("alive:pid",pid)
        # Set string pid:port
        self.redis.set("pid:"+pid,port)
        # Set string expire time
        self.redis.expire("pid:"+pid,expire)
    def rprocstop(self,pid):
        # Move pid to dead pid
        self.redis.smove("alive:pid","dead:pid",pid)
        # Find the key corresponding to pid
        for token in self.redis.hkeys("token:pid"):
            if self.redis.hget("token:pid",token) == pid:
                # Delete the key in hash table
                self.redis.hdel("token:pid",token)
                self.redis.hdel("token:port",token)
                # Recycle the port
                self.redis.smove("alive:port","dead:port",self.redis.hget("token:port",token))
                break
        prockill(int(pid),9)
    def isrunning(self,token):
        # Find the token in hash table
        pid = self.redis.hget("token:pid",token)
        if pid != None:
            return self.isalivepid(pid)
        else:
            return False
    def isalivepid(self,pid):
        return pathexists("/proc/"+str(pid))
    def update(self,token,expire=600):
        self.redis.expire("pid:"+self.redis.hget("token:pid",token),expire)
    def checkalivepid(self):
        for pid in self.redis.smembers("alive:pid"):
            if self.redis.get("pid:"+pid) == None:
                self.rprocstop(pid)
    def getalivepid(self):
        return self.redis.smembers("alive:pid")
    def rprockill(self,token):
        pid = self.redis.hget("token:pid",token)
        self.rprocstop(pid)
    def getportbytoken(self,token):
        port = self.redis.hget("token:port",token)
        return port
    def commandflood(self,token):
        token = "token:" + token
        if self.redis.scard(token) == 0:
            self.redis.sadd(token,time.ctime())
            self.redis.expire(token,60)
        elif self.redis.scard(token) >= 30:
            return False
        else :
            self.redis.sadd(token,time.ctime())
        return True

