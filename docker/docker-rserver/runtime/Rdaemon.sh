#!/bin/bash

RDaemonCore="lib/RDaemonCore.py"
RDaemon="lib/RDaemon.py"

if [ ! -d "lib" ]; then
    cd /data/
    RDaemonCore="RDaemonCore.py"
    RDaemon="RDaemon.py"
fi

echo $RDaemonCore

function init(){
    echo "Init ..."
    pkill -9 -f RDaemon.py
    pkill -9 -f RDaemonCore.py
    pkill -9 -f server.R
    sleep 1
}

function restart(){
    if [ $# -eq 1 ]; then
        debug="T"
    else
        debug="F"
    fi
    pkill -9 -f RDaemon.py
    pkill -9 -f RDaemonCore.py
    pkill -9 -f server.R
    sleep 1
    netstat -ltap |grep -E "60010|60011"
    if [ $debug = "T" ]; then
        echo "Restarting Rmanager with debug mode... ..."
        python $RDaemonCore &
        echo "Restarting Rclient with debug mode... ..."
        python $RDaemon &
    else
        echo "Restarting Rmanager ... ..."
        nohup python $RDaemonCore 2>&1 & 
        echo "Restarting Rclient ... ..."
        nohup python $RDaemon  2>&1 &
    fi
}

function Rdaemon(){
    pid=`ps -ef|grep $1|grep -v grep|wc -l`
    if [ $pid -eq 0 ]; then
        echo $1 $pid
        restart
        sleep 1
    fi
}

init
while [ 1 == 1 ]
do
    Rdaemon "RDaemonCore.py" 
    Rdaemon "RDaemon.py" 
    sleep 1
done
