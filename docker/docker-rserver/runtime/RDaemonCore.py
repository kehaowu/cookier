import os, socket

if __name__ == '__main__': 
    if socket.gethostname() != "localhost.localdomain":
        os.chdir("/data/")
    from lib.rserver import rserver
    rserver = rserver()
    rserver.run()
