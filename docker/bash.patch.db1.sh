#!/bin/bash

RED='\033[0;31m' # Red color
GREEN='\033[0;32m' # Green color
NC='\033[0m' # No Color

warn () {
  echo -e $RED $@ $NC
}

tell () {
  echo -e "\n$GREEN $@ $NC"
}

cookiermysqlip=`docker inspect cookier-mysql|grep "\"IPAddress\""|head -n 1 |cut -d '"' -f4`
mysqluser=cookier
mysqlpass=HxQIa4WjQl7wKwJp82fA6sWEE1GvfQkD
mysqldb=cookier
tell $mysqluser $mysqldb $cookiermysqlip
if [ $1 == "debug" ]; then
   tell Trying to connect cookier
   mysql -h$cookiermysqlip -u$mysqluser -p$mysqlpass $mysqldb
elif [ $1 == "backup" ]; then
   tell Trying to backup mysql
   mysqldump -h$cookiermysqlip -u$mysqluser -p$mysqlpass $mysqldb > /data/cookier/backup/`date "+%Y%m%d%H%M%S"`.bk
else
   tell [`date`] patch cookier mysql database from $cookiermysqlip 
   mysql -h$cookiermysqlip -u$mysqluser -p$mysqlpass $mysqldb < /data/cookier/sql/cookier_patch_${1}.sql
   echo Done!
fi

