#!/bin/bash
dir=`pwd`
dockername=$1
echo Start docker with base dir: $dir
echo Init supervisor log at $dir/supervisor/$dockername
mkdir -p $dir/supervisor/$dockername

dockerid=`docker run \
    --name $dockername \
    -p 127.0.0.1:3388:3388 \
    -d \
    --link cookier-mysql:mysql \
    --link rserver_1:rserver_1 \
    --link cookier-redis:redis \
    -v /data/cookier:/data \
    -v $dir/supervisor/$dockername:/var/log/supervisor \
    cookier/tornado`
ip=`docker inspect $dockerid|grep IPAddress|cut -d '"' -f4`
echo $dockername running with IP:$ip
