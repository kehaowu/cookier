#!/bin/bash

docker run \
    --name cookier-tornado \
    -p 127.0.0.1:3388:3388 \
    -d \
    --link cookier-mysql:mysql \
    --link cookier-redis:redis \
    -v /data/cookier:/data \
    cookier/tornado \
    /data/main.py
