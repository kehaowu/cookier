#!/usr/bin/env python
import os, socket
import base64, uuid
import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado.options
import torndb
import tornado

from tornado.web import RequestHandler
from tornado.web import traceback
from tornado.options import define, options
from redis import Redis as redis
from lib.config.getConfig import MysqlConfig
from lib.pages import console,client
from lib.apis import cmd,sim,auth,apis,task

from admin  import apis as adminapis
from admin  import auth as adminauth
from admin  import pages as adminpages
if socket.gethostname() != "localhost.localdomain":
    os.chdir("/data/")
define("port", default=3388, help="run on the given port", type=int)

cookie_sec = 'yHKPTuoj9sKEsmGsSc0R4TSyiKzOScTvTfy'

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
                    ### Front-end

                    # Pages
                    (r"/", client.indexHandler),
                    (r"/course/(\w+)", client.chapterListHandler),
                    (r"/class/(\w+)", console.consoleHandler),
                    (r"/simulate", sim.simHandler),
                    (r"/forgetpassword", client.forgetPasswdHandler),
                    (r"/passwdreset/(\w+)", client.passwdResetHandler),
                    (r"/task", client.taskHandler),
                    (r"/indeximg/([0-4]\.png)", tornado.web.StaticFileHandler, dict(path=os.path.join(os.path.dirname(__file__), "client/dist/img/"))),
                    
                    # Apis
                    (r"/api/cmd", cmd.cmdHandler),
                    (r"/api/init/(\w+)", cmd.initHandler),
                    (r"/api/signin", auth.SignInHandler),
                    (r"/api/signup", auth.SignUpHandler),
                    (r"/api/forgetpassword", auth.ForgetHandler),
                    (r"/api/passwdreset", auth.PasswdResetHandler),
                    (r"/api/recharge", apis.RechargeHandler),
                    (r"/api/process/(\w+)", apis.ProcessHandler),
                    (r"/api/feedback", apis.FeedbackHandler),
                    (r"/api/course", apis.CourseHandler),
                    (r"/api/class/new/(\d+)", apis.NewClassHandler),
                    (r"/api/course/(\w+)", apis.CourseByIdHandler),
                    (r"/api/content/(\w+)/course", apis.CourseByContentIdHandler),
                    (r"/api/record/(\w+)/([01])", apis.RecordHandler),
                    (r"/api/task", task.TaskHandler),
                    (r"/api/task/claim/(\w+)", task.ClaimTaskHandler),
                    (r"/api/task/(\w+)", task.TaskDetailsHandler),
                    (r"/api/user/task", task.TaskPerUserHandler),                    
                    (r"/api/content/(\w+)/(\w+)", apis.ContentEvaluationHandler),                    

                    # Admin Apis
                    (r"/admin/api/signup", adminauth.SignUpHandler),                             # GET ALL course; POST Create new course
                    (r"/admin/api/signin", adminauth.SignInHandler),                             # GET ALL course; POST Create new course
                    (r"/admin/api/course", adminapis.courseHandler),                             # GET ALL course; POST Create new course
                    (r"/admin/api/course/(\w+)", adminapis.courseByidHandler),                 # GET Details; POST Modify; DELETE Delete
                    (r"/admin/api/course/(\w+)/chapter", adminapis.chapterByCourseidHandler),    # GET Chapter list; POST Create new chapter
                    (r"/admin/api/chapter/(\w+)", adminapis.chapterByidHandler),               # GET Details; POST Modify; DELETE Delete
                    (r"/admin/api/chapter/(\w+)/content", adminapis.contentByChapteridHandler),  # GET Content list; POST Create new content
                    (r"/admin/api/content/(\w+)", adminapis.contentByidHandler),               # GET Details; POST Modify; DELETE Delete
                    (r"/admin/api/recharge/", adminapis.rechargeHandler),               # GET Details; POST Modify; DELETE Delete

                    # Admin Pages
                    (r"/admin/course/(\w+)", adminpages.CourseHandler),
                    (r"/admin", adminpages.IndexHandler),
                    (r"/admin/recharge", adminpages.rechargeHandler),

                    # 404 Error
                    (r"/.*", client.pageNotFoundHandler),
                    ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "client/dist/html/"),
            static_path=os.path.join(os.path.dirname(__file__), "client/dist/"),
            ui_modules={
            },
            xsrf_cookies=False,
            cookie_secret=cookie_sec,
            debug=True,
        )
        super(Application,self).__init__(handlers, **settings)
        mysqlcfg = MysqlConfig('main')
        self.db = torndb.Connection(
                host='%s:%s'%(mysqlcfg.host,mysqlcfg.port), database=mysqlcfg.database,
                user=mysqlcfg.user, password=mysqlcfg.passwd, 
                time_zone=mysqlcfg.time_zone, charset=mysqlcfg.charset)
        rediscfg = MysqlConfig('redis')
        self.redis = redis(host=rediscfg.host,port=rediscfg.port,db=rediscfg.database)

if __name__ == "__main__":
    print 'START RUNNING SERVER ON PORT ' + str(options.port)
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


