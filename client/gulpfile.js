//------------------------------------------------------------------------------
// Plugins
var gulp = require('gulp');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var reactify = require('reactify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var glob = require('glob');
var del = require('del');
var streamify = require('gulp-streamify')
//------------------------------------------------------------------------------
// Config
path = {
    js:'./src/',
    app_js:'./dist/js/build'
};


//------------------------------------------------------------------------------
//Functions
var makeReactify = function(path){
    var jsfiles = glob.sync(path.js+"*.js");
    jsfiles.forEach(function(file){
        var destfile = file.match(/\w+\.js/g)[0]
        console.log("Reactifying "+destfile)
        browserify(file)
        .transform(reactify)
        .bundle()
        .pipe(source(destfile))
        .pipe(streamify(uglify()))
        .pipe(gulp.dest(path.app_js))
    })
};


//------------------------------------------------------------------------------
// Tasks clean
gulp.task('clean', function(done) {
    del(['./dist/js/build'], done);
});

//------------------------------------------------------------------------------
// Tasks js
gulp.task ('js',  function() {
    makeReactify(path)
});


//------------------------------------------------------------------------------
// Tasks watch
gulp.task('watch', function() {
    gulp.watch("./src/*.js", ['js']);
    gulp.watch("./src/components/*.js", ['js']);
})

//------------------------------------------------------------------------------
// The default task (called when we run `gulp` from cli)
gulp.task('default', [ 'watch']);
