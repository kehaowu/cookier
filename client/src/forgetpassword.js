/** @jsx React.DOM */
var React = require('react');
var ForgetPassword = require('./components/ForgetPassword.js');
var Feedback = require('./components/Feedback.js');

var App = React.createClass({
    render: function(){
        return (
            <div>
                <ForgetPassword />
                <Feedback />
                <p className = "text-right">Rzaixian.com <a data-toggle="modal" data-target="#feedback">反馈</a></p>
            </div>
            )
    }
})


React.render(
      <App />,
      document.getElementById('app')
);

