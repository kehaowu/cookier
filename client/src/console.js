/** @jsx React.DOM */
var React = require('react');
var SignInUp = require('./components/SignInUp.js');
var Navbar = require("./components/Navbar.js");
var Footer = require("./components/Footer.js");
var Console = require("./components/Console.js");
var Blackboard = require("./components/Blackboard.js");
var CourseProcess = require("./components/CourseProcess.js");

var App = React.createClass({
    getInitialState: function() {
        var contentid = window.location.pathname.split("/")[2]
        return {
            signIn : true,
            token : "cookiertoken",
            contentid: contentid,
            data: []
        }
    },
    componentDidMount: function() {
        var token = this.state.token
        var part = 1
        if (localStorage.getItem(token) != undefined){
            $("#console-input").width($("#console-command-line").width()*0.95-$("#console-prompt").width())
            var totalHeight = $(window).height()
            var top = $("#app-console").offset().top 
            $("#console-blackboard").outerHeight((totalHeight - top - 16) / part)
            $("#console").outerHeight((totalHeight - top - 16) / part)
        }
        window.history.pushState(null, null, "/class/" + this.state.contentid )
    },
    clickHandler: function() {
        this.setState({
            signIn : !this.state.signIn
        })
    },
    updateClickHandler: function(event) {
        var contentid = event.target.parentNode.getAttribute('data-url')
        window.history.pushState(null, null, "/class/" + contentid )
        this.setState({
            contentid: contentid
        })
    },
    render: function() {
        var token = this.state.token
        if (localStorage.getItem(token) == undefined)
        {
            var dom = (
                    <div style = {{ paddingTop: "4em", marginBottom: "5em" }} className="row">
                        <div className = "col-md-3 col-md-offset-8">
                            <SignInUp />
                        </div>
                    </div>
                )
        }else{
            var userinfo = JSON.parse(window.localStorage.getItem(this.state.token))
            var url = "/api/content/" + this.state.contentid + "/course"
            var dom = (
                <div>
                    <CourseProcess
                        token = {this.state.token}
                        contentid = { this.state.contentid }
                        updateClick = { this.updateClickHandler }
                        url = {url} />
                    <div className="row" id="app-console">
                        <div className="col-md-6 col-lg-6">
                            <Blackboard token = {this.state.token} contentid = { this.state.contentid }/>
                        </div>
                        <div className="col-md-6 col-lg-6">
                            <Console token = {this.state.token}  contentid = { this.state.contentid } />
                        </div>
                    </div>
                </div>
                )
        }
        
        return (
                <div>
                    <Navbar
                        logo = "R语言在线"
                        token = {token}
                        callback = "/" />
                    <div className = "cookier">
                        { dom }
                    </div>
                    <Footer token = {token} />
                </div>
            )
    }
});

React.render(
      <App />,
      document.getElementById('app')
);
