/** @jsx React.DOM */
var React = require('react');
var SignInUp = require('./components/SignInUp.js');
var Navbar = require("./components/Navbar.js");
var TaskList = require("./components/Task/TaskList.js");
var TaskDetail = require("./components/Task/TaskDetail.js")
var TaskPerUser = require("./components/Task/TaskPerUser.js")
var TaskForm = require("./components/Task/TaskForm.js")
var Footer = require("./components/Footer.js")

var App = React.createClass({
    getInitialState: function() {
        return {
            left: true,
            detail: [],
            contact: [],
            token: "cookiertoken",
            switcher: 0
        }
    },
    leftClick: function(){
        this.setState({
            left: !this.state.left
        })
    },
    itemClick: function(id){
        $.ajax({
            url:"/api/task/" + id,
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.state.token)).token
            },
            success:function(data){
                if(data.status == "true"){
                    this.setState({
                        detail: data.data,
                        contact: data.contact,
                    })
                }
            }.bind(this),
        })
    },

    render: function() {
        var token = this.state.token
        var domleft = this.state.left == true ? <TaskList headerClick = { this.leftClick } token = {token} itemClick = {this.itemClick}/> : <TaskPerUser headerClick = { this.leftClick } token = {token} itemClick = {this.itemClick}/>
        var domright0 = this.state.detail == [] ? <div /> : <TaskDetail data = { this.state.detail } contact = {this.state.contact}/>
        var domright = window.localStorage.getItem(token) == undefined ? <div className = "col-md-10 col-md-offset-1"><SignInUp /></div> : domright0
        return (
                <div>
                    <Navbar
                        logo = "R语言在线"
                        token = {token}
                        callback = "/" />
                    <div className = "cookier">
                        <div className = "row">
                            <div className = "col-md-12">
                                <TaskForm token = { token } />
                                <button className = "btn btn-xs btn-default" data-toggle="modal" data-target="#form">发布新任务</button>
                            </div>
                        </div>
                        <div className = "row">
                            <div className = "col-md-6">
                                { domleft }
                            </div>
                            <div className = "col-md-6">
                                { domright }
                            </div>
                        </div>
                        
                    </div>
                    <Footer token = {token} />
                </div>
            )
    }
});

React.render(
      <App />,
      document.getElementById('app')
);
