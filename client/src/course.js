/** @jsx React.DOM */
var React = require('react');
var SignInUp = require('./components/SignInUp.js');
var Navbar = require("./components/Navbar.js");
var Footer = require("./components/Footer.js");
var DeleteButton = require("./components/DeleteButton.js")
var CourseDetails = require("./components/CourseDetails.js")

var App = React.createClass({
    getInitialState: function() {
        return {
            signIn : true,
            token : "cookiertoken",
            data: []
        }
    },
    clickHandler: function() {
        this.setState({
            signIn : !this.state.signIn
        })
    },
    render: function() {
        var token = this.state.token
        if (localStorage.getItem(token) == undefined)
        {
            var dom = (
                    <div style = {{ paddingTop: "4em", marginBottom: "5em" }} className="row">
                        <div className = "col-md-3 col-md-offset-8">
                            <SignInUp />
                        </div>
                    </div>
                )
        }else{
            var url = "/api" + window.location.pathname
            var dom = (
                <div className="row">
                    <div className = "col-md-8 col-md-offset-2">
                        <CourseDetails 
                            token = {this.state.token}
                            url = {url}
                            courseDetails = "show"
                            blank = "_blank"/>
                    </div>
                </div>
                )
        }
        
        return (
                <div>
                    <Navbar
                        logo = "R语言在线"
                        token = {token}
                        callback = "/" />
                    <div className = "cookier" style = {{ marginTop: "6em" }}>
                        { dom }
                    </div>
                    <Footer token = {token} />
                </div>
            )
    }
});

React.render(
      <App />,
      document.getElementById('app')
);
