/** @jsx React.DOM */
var React = require('react');
var SignInUp = require('./components/SignInUp.js');
var Navbar = require("./components/Navbar.js");
var Footer = require("./components/Footer.js");
var DeleteButton = require("./components/DeleteButton.js")
var CourseList = require("./components/CourseList.js")
var CourseDetails = require("./components/CourseDetails.js")
var NewClass = require("./components/NewClass.js")

var App = React.createClass({
    getInitialState: function() {
        return {
            token : "cookiertoken",
            data: []
        }
    },
    render: function() {
        var token = this.state.token
        var imgDom = (<div className="jumbotron" style = {{ textAlign:'center', height: "267px" }}>
                    <h1>精美教程 + 在线实操</h1>
                    <a href = "/#allcourses" className = "btn btn-primary btn-lg btn-shadow" style = {{ marginTop: '2em' }}>开始学习</a>
                </div>
            )
        if (localStorage.getItem(token) == undefined)
        {
            var dom = (
                <div>
                    <div
                        className="row">
                        <div
                            className = "col-md-9">
                            {imgDom}
                        </div>
                        <div className = "col-md-3">
                            <SignInUp />
                        </div>
                    </div>
                    <div className = "row">
                        <div style = {{ borderTop: "1px solid #DDD", paddingTop: "2.4em" }} className = "col-md-9">
                            <CourseList/>
                        </div>
                        <div style = {{ paddingTop: "2.4em" }} className = "col-md-3">
                            <NewClass/>
                        </div>
                    </div>
                </div>)
        }else{
            var userinfo = JSON.parse(window.localStorage.getItem(token))
            var dom = (
                <div className="row" id="allcourses">
                    <div className = "col-md-9">
                        <h4 style = {{ borderBottom: "1px solid #DDD" }}>所有课程</h4>
                        <CourseList token = {this.state.token}/>
                    </div>
                    <div className = "col-md-3">
                        <NewClass/>
                    </div>
                </div>
                )
        }
        return (
                <div>
                    <Navbar
                        logo = "R语言在线"
                        token = {token}
                        callback = "/" />
                    <div className = "cookier">
                        { dom }
                    </div>
                    <Footer token = {token} />
                </div>
            )
    }
});

React.render(
      <App />,
      document.getElementById('app')
);
