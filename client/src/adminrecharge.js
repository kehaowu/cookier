/** @jsx React.DOM */
var React = require('react');
var AdminSignInUp = require('./components/AdminSignInUp.js');
var Navbar = require("./components/Navbar.js");
var CreateRecharge = require("./components/CreateRecharge.js");
var RechargeList = require("./components/RechargeList.js");

var App = React.createClass({
    getInitialState: function() {
        return {
            signIn : true,
            token : "admintoken",
            data: []
        }
    },
    clickHandler: function() {
        this.setState({
            signIn : !this.state.signIn
        })
    },
    loadData: function() {
        var token = this.state.token
        if(localStorage.getItem(token) != undefined){
            var url = "/admin/api/course"
            $.ajax({
                url:url,
                type:"GET",
                dataType:"json",
                headers:{
                    Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.state.token)).token
                },
                success:function(data){
                    data.data.push({
                        title:"添加新课程",
                        courseid: 0
                    })
                    this.setState({
                        'data':data.data
                    })
                }.bind(this),
                error:function(){
                }.bind(this),
                async:true
            });
        }
    },
    componentWillMount:function(){
        this.loadData()
    },
    render: function() {
        var token = this.state.token
        if (localStorage.getItem(token) == undefined)
        {
            var dom = (<div
                    className = "col-md-3">
                    <AdminSignInUp 
                        signinurl = "/admin/api/signin"
                        signupurl = "/admin/api/signup"
                        token = {token}
                        callback = "/admin"/>
                </div>)
        }else{
            var dom = (
                <div>
                    <div className = "row">
                        <div className = "col-md-12">
                            <CreateRecharge 
                                token = {token}/>
                        </div>
                    </div>
                    <div className = "row">
                        <div className = "col-md-12">
                            <RechargeList 
                                token = {token}/>
                        </div>
                    </div>
                </div>
                )
        }
        
        return (
                <div>
                    <Navbar
                        logo = "数据学院管理员后台"
                        token = {token}
                        callback = "/admin" />
                    <div className = "cookier">
                    { dom }
                    <p><a
                            className = "text-danger text-right"
                            href = "/admin/recharge"
                        >
                        充值卡管理
                    </a></p>
                    </div>
                </div>
            )
    }
});

React.render(
      <App />,
      document.getElementById('app')
);
