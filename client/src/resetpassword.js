/** @jsx React.DOM */
var React = require('react');
var ResetPassword = require('./components/ResetPassword.js');
var Feedback = require('./components/Feedback.js');

var App = React.createClass({
    render: function(){
        return (
            <div>
                <ResetPassword />
                <Feedback />
                <p className = "text-right">Rzaixian.com<a data-toggle="modal" data-target="#feedback">反馈</a></p>
            </div>
            )
    }
})


React.render(
      <App />,
      document.getElementById('app')
);

