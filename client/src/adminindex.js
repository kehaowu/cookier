/** @jsx React.DOM */
var React = require('react');
var AdminSignInUp = require('./components/AdminSignInUp.js');
var Navbar = require("./components/Navbar.js");
var DeleteButton = require("./components/DeleteButton.js")
var CourseEditFrame = require("./components/CourseEditFrame.js")

var App = React.createClass({
    getInitialState: function() {
        return {
            signIn : true,
            token : "admintoken",
            data: [],
            editable: false,
            courseDetails:{
                title:"尚未加载数据",
                description:"尚未加载数据"
            }
        }
    },
    clickHandler: function() {
        this.setState({
            signIn : !this.state.signIn
        })
    },
    addClick: function() {
        this.setState({
            editable: true
        })
    },
    submitAddCourse: function(event){
        var url = "/admin/api/course"
        if(event.keyCode == 13){
            $.ajax({
                url: url,
                type:"POST",
                headers:{
                    Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.state.token)).token
                },
                data:{
                    'data':JSON.stringify({
                        'title':event.target.value,
                        'description':" "
                    })
                },
                dataType:"json",
                success:function(data){
                    if(data.status == "true"){
                        this.loadData()
                        this.setState({
                            editable: false
                        })
                    }
                }.bind(this),
                error:function(data){
                }
            })
        }
    },
    loadData: function() {
        var token = this.state.token
        if(localStorage.getItem(token) != undefined){
            var url = "/admin/api/course"
            $.ajax({
                url:url,
                type:"GET",
                dataType:"json",
                headers:{
                    Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.state.token)).token
                },
                success:function(data){
                    data.data.push({
                        title:"添加新课程",
                        courseid: 0
                    })
                    this.setState({
                        'data':data.data
                    })
                }.bind(this),
                error:function(){
                }.bind(this),
                async:true
            });
        }
    },
    componentWillMount:function(){
        this.loadData()
    },
    deleteClick: function(id) {
        $.ajax({
            url:"/admin/api/course/"+id,
            type:"DELETE",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.state.token)).token
            },
            success:function(data){
                this.loadData()
            }.bind(this),
            error:function(){
            }
        })
    },
    editClick: function(id) {
        $.ajax({
            url:"/admin/api/course/" + id,
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.state.token)).token
            },
            success:function(data){
                this.setState({
                    'courseDetails':data.data[0]
                })
            }.bind(this),
            error:function(data){
            }.bind(this),
            async:true
        });
    },
    render: function() {
        var token = this.state.token
        if (localStorage.getItem(token) == undefined)
        {
            var dom = (<div
                    className = "col-md-3">
                    <AdminSignInUp 
                        signinurl = "/admin/api/signin"
                        signupurl = "/admin/api/signup"
                        token = {token}
                        callback = "/admin"/>
                </div>)
        }else{
            var courseList = this.state.data.map(function(item,i){
                var url = "/admin/course/" + item.courseid
                key = item.courseid + i + item.title
                if(item.courseid != 0){
                    var dom = (<li
                            className = "list-group-item">
                                <span
                                    style ={
                                        {cursor: "pointer"}
                                    }
                                    key = {key}
                                    onClick = {this.editClick.bind(null,item.courseid)}
                                >
                                {item.title}
                                </span>
                                <a
                                    style = {
                                        {
                                            marginLeft:"1em",
                                            color: "#DDD"
                                        }
                                    }
                                    key = { key + "B" }
                                    href={ url }
                                    target = "_blank"
                                >
                                    修改课程章节
                                </a>
                                <DeleteButton
                                    className = "btn"
                                    onClick = { this.deleteClick.bind(null,item.courseid) }/>
                        </li>)
                }else{
                    if(this.state.editable == true){
                        var style = {
                            width: "100%",
                            backgroundColor:"#EEE"
                        }
                        var dom = (<input 
                                key = {key}
                                style = {style}
                                className = "list-group-item"
                                onKeyDown = {this.submitAddCourse}>
                            </input>)
                    }else{
                        var dom = <a key = {key} className = "list-group-item" onClick = {this.addClick}> 添加新课程 </a>
                    }
                }
                return dom
            }.bind(this))
            var userinfo = JSON.parse(window.localStorage.getItem(this.state.token))
            var dom = (
                <div className="row">
                    <h1>{userinfo.username}， 你好！ 欢迎来到后台！</h1>
                    <ul className = "list-group col-md-4">
                        <li className = "list-group-item active">
                            课程列表
                        </li>
                        { courseList }
                    </ul>
                    <CourseEditFrame
                        token = {this.state.token}
                        key = {this.state.courseDetails.courseid}
                        className = "col-md-8"
                        data = {this.state.courseDetails} />
                </div>
                )
        }
        
        return (
                <div>
                    <Navbar
                        logo = "数据学院管理员后台"
                        token = {token}
                        callback = "/admin" />
                    <div className = "cookier">
                    { dom }
                    <p><a
                            className = "text-danger text-right"
                            href = "/admin/recharge"
                        >
                        充值卡管理
                    </a></p>
                    </div>
                </div>
            )
    }
});

React.render(
      <App />,
      document.getElementById('app')
);
