/** @jsx React.DOM */
var React = require('react');
var md5 = require('md5');

var AdminSignUp = React.createClass({
    loginClick: function(event){
        $.ajax({
            url:this.props.url,
            type:"POST",
            dataType:"json",
            data : {
                    username : this.refs.username.getDOMNode().value,
                    email : this.refs.email.getDOMNode().value,
                    password : md5(this.refs.password.getDOMNode().value),
                    token : this.refs.token.getDOMNode().value
            },
            success:function(data){
                localStorage.setItem(this.props.token, JSON.stringify(data));
                if (localStorage.getItem(this.props.token) != undefined){
                    window.location.assign(this.props.callback)
                }
            }.bind(this),
            error: function(data){
            }.bind(this)
        })
    },
    render: function() {
        var aStyle = {color : "#FF0000"}
        return (
            <div className = "panel panel-success">
                <div>
                    <div className = "signinup-heading" onClick = {this.props.onClick}>登陆</div>
                    <div className = "signinup-heading signinup-heading-active">注册</div>
                </div>
                <div className = "panel-body">
                    <form>
                        <div className = "form-group">
                            <label> 用户名 </label>
                            <input 
                                ref = "username"
                                type="text"
                                className = "form-control"
                            >
                            </input>
                        </div>
                        <div className = "form-group">
                            <label> 邮箱 </label>
                            <input 
                                ref = "email"
                                type="email"
                                className = "form-control"
                            >
                            </input>
                        </div>
                        <div className = "form-group">
                            <label> 密码 </label>
                            <input
                                ref = "password"
                                type="password"
                                className = "form-control"
                            >
                            </input>
                        </div>
                        <div className = "form-group">
                            <label> 注册令牌 </label>
                            <input
                                ref = "token"
                                type="password"
                                className = "form-control"
                            >
                            </input>
                        </div>
                        <div className = "form-group">
                            <input 
                                type="button" 
                                className = "btn btn-success btn-block"
                                onClick = {this.loginClick}
                                value = "注册"
                            >
                            </input>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
    
});
    
module.exports = AdminSignUp;