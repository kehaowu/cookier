/** @jsx React.DOM */
var React = require('react');

DeleteButton = React.createClass({
    getInitialState: function(){
        return ({
            comfirm:false
        })
    },
    cancelClick: function() {
        this.setState({
            comfirm: false
        })
    },
    evokeClick: function() {
        this.setState({
            comfirm: true
        })
    },
    render: function(){
        var style = {
            display: "inline"
        }
        if(this.state.comfirm == true){
            var dom =  <div style = {style} >
                        <button
                            className = "btn delete-button delete-button-comfirm"
                            onClick = {this.props.onClick}
                        >
                            确认
                        </button>
                        <button
                            className = "btn delete-button delete-button-comfirm"
                            onClick = {this.cancelClick}
                        >
                            取消
                        </button>
                    </div>
        }else{
            var dom  = <button
                        className = "btn delete-button"
                        onClick = {this.evokeClick}
                    >
                        x
                    </button>
        }
        return dom
    }
});

module.exports = DeleteButton;