/** @jsx React.DOM */
var React = require('react');

CollapseButton = React.createClass({
    render: function(){
        var label = this.props.collapse ? "展开" : "收起"
        return(
                <button
                    className = "btn collapse-button"
                    onClick = {this.props.onClick}
                >
                    { label }
                </button>
            )
    }
});

module.exports = CollapseButton;