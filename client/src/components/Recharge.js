/** @jsx React.DOM */
var React = require('react');

var Recharge = React.createClass({
    getInitialState: function(){
        return ({
            active: false,
            fail: false
        })
    },
    rechargeClick: function(){
        $.ajax({
            url:"/api/recharge",
            type:"POST",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            data:{
                'data':JSON.stringify({
                    'card':this.refs.card.getDOMNode().value
                })
            },
            success:function(data){
                if(data.status == "true"){
                    this.setState({
                        active: true,
                        fail: false
                    })
                }else{
                    this.setState({
                        active: false,
                        fail: true
                    })
                }
            }.bind(this),
            error:function(){
            }
        })
    },
    activeClick: function(){
        this.setState({
            active: true,
            fail: true
        })
    },
    gobackClick: function(){
        this.setState({
            active: false,
            fail: false
        })
    },
    render: function(){
        if(this.state.active == true){
            if(this.state.fail == true){
                var dom = (
                        <form className = "form-inline">
                            <div className = "form-group">
                                <span>请输入卡号：</span>
                                <input className = "form-control" ref = "card" />
                                <button type = "button" className = "btn btn-default" onClick = {this.rechargeClick}>充值</button>
                            </div>
                        </form>
                    )
            }else{
                var dom = (
                        <p>充值成功！<a onClick = {this.gobackClick}>返回</a></p>
                    )
            }
        }else{
            if(this.state.fail == true){
                var dom = (
                        <p>充值失败，如有需要请联系客服！<a onClick = {this.gobackClick}>返回</a></p>
                    )
            }else{
                var dom = (
                        <a onClick = {this.activeClick}>我要充值</a>
                    )
            }
        }
        return (
            <div>
                {dom}
            </div>
            )
    }
});

module.exports = Recharge;
