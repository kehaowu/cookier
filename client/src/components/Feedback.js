/** @jsx React.DOM */
var React = require('react');

var Feedback = React.createClass({
    getInitialState: function() {
        return {
            success: false,
            submit: false,
            click: false
        }
    },
    submitClickHandler: function(){
      this.setState({
        click: true
      })
      $.ajax({
          url:"/api/feedback",
          type:"POST",
          dataType:"json",
          data : {
              'data':JSON.stringify({
                  email : this.refs.email.getDOMNode().value,
                  subject : "Feedback:" + this.refs.subject.getDOMNode().value,
                  content : this.refs.content.getDOMNode().value
                })
          },
          success:function(data){
              if (data.status == "true"){
                this.setState({
                    success: true,
                    click: false
                })
              } else {
                this.setState({
                    submit: true,
                    click: false
                })
              }
          }.bind(this),
          error: function(data){
            this.setState({
                submit: true,
                click: false
            })
          },
          async: true
      })
    },
    render: function() {
      var dom = (
                <form>
                  <div className="form-group">
                    <label htmlFor="subject">标题</label>
                    <input type="text" className="form-control" ref = "subject" placeholder="标题" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">联系方式</label>
                    <input type="email" className="form-control" ref = "email" placeholder="电子邮件" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="content">意见与建议内容</label>
                    <textarea className="form-control" rows="5" ref = "content" />
                  </div>
                </form>
        )
        if(this.state.success == true){
          var dom = <h4>反馈成功！我们会尽快与您联系！</h4>
        } else {
          if(this.state.click == true){
            var dom = <h4>正在提交……</h4>
          }
          if(this.state.submit == true){
            var dom = <h4>提交失败。</h4>
          }
        }
      return(
        <div className="modal fade" id = "feedback" tabIndex="-1" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">意见与建议</h4>
              </div>
              <div className="modal-body">
                  { dom }
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" className="btn btn-primary" onClick = { this.submitClickHandler }>提交</button>
              </div>
            </div>
          </div>
        </div>
          )
    }
});

module.exports = Feedback;

