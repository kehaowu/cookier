/** @jsx React.DOM */
var React = require('react');
var SignIn = require('./SignIn.js');
var SignUp = require('./SignUp.js');

var SignInUp = React.createClass({
    getInitialState: function() {
        return {
            signIn : false,
            token : "cookiertoken",
        }
    },
    clickHandler: function() {
        this.setState({
            signIn : !this.state.signIn
        })
    },
    render: function(){
            var callback = this.props.callback == undefined ? "/" : this.props.callback
            var token = this.props.token == undefined ? this.state.token : this.props.token
            var signinurl = this.props.signinurl == undefined ? "/api/signin" : this.props.signinurl
            var signupurl = this.props.signupurl == undefined ? "/api/signup" : this.props.signupurl
            if (this.state.signIn == true){
                dom = (<div>
                                <SignIn
                                    url = {signinurl}
                                    token = { token }
                                    onClick = {this.clickHandler} callback = {callback} />
                              </div> )
            }else{
                dom = (<div>
                                <SignUp 
                                    url = {signupurl}
                                    token = { token }
                                    onClick = {this.clickHandler} callback = {callback} />
                                </div>)
            }
            return <div>{dom}</div>
    }
})

module.exports = SignInUp;
