/** @jsx React.DOM */
var React = require('react');

var CreateRecharge = React.createClass({
    getInitialState: function(){
        return ({
            success: false,
            cardid: ""
        })
    },
    addClick: function(id){
        $.ajax({
            url:"/admin/api/recharge/",
            type:"POST",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            data:{
                'data':JSON.stringify({
                    'credit':id
                })
            },
            success:function(data){
                if(data.status = "true"){
                    this.setState({
                        success: true,
                        cardid: data.data.cardid
                    })
                }
            }.bind(this),
            error:function(){
            }
        })
    },
    gobackClick: function(){
        this.setState({
            success: false,
            cardid: ""
        })
    },
    render: function(){
        if (this.state.success == true){
            var dom = <h5>添加成功，卡号：{this.state.cardid}，<a onClick = {this.gobackClick} >返回</a></h5>
        }else{
            var dom = (
                <div>
                    <span>创建充值卡：</span>
                    <button className = "btn btn-default" onClick = {this.addClick.bind(null,50)}> 50分 </button>
                    <button className = "btn btn-default" onClick = {this.addClick.bind(null,100)}> 100分 </button>
                    <button className = "btn btn-default" onClick = {this.addClick.bind(null,300)}> 300分 </button>
                    <button className = "btn btn-default" onClick = {this.addClick.bind(null,500)}> 500分 </button>
                    <button className = "btn btn-default" onClick = {this.addClick.bind(null,1000)}> 1000分 </button>
                </div>
                )
        }
        return (
            <div>
                {dom}
            </div>
            )
    }
});

module.exports = CreateRecharge;
