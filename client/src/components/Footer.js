/** @jsx React.DOM */
var React = require('react');
var Recharge = require("./Recharge.js")
var Feedback = require("./Feedback.js")

var Footer = React.createClass({
    render: function() {
        return (
            <footer>
                <Feedback />
                <div className = "row">
                    <div className = "col-md-6">
                        <p className = "text-left">
                            Copyright ©  2016  版权所有，严禁转载   <a data-toggle="modal" data-target="#feedback">www.Rzaixian.com 公测版</a> 
                        </p>
                    </div>
                        <p className = "text-right" style = {{ paddingRight: "1em" }} >
                            <a data-toggle="modal" data-target="#feedback">意见与建议</a> /  
                            <a href = "http://r.rzaixian.com">博客</a> / 
                            <a href = "http://bigdata.rzaixian.com">大数据</a> / 
                            <a href = "mailto:business@rzaixian.com">业务联系</a> /
                            <a href = "mailto:business@rzaixian.com">加入我们</a> 
                        </p>
                    <div className = "col-md-6">
                    </div>
                </div>
            </footer>
            )
    }
});

module.exports = Footer;