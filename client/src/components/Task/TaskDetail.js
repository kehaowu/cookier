/** @jsx React.DOM */
var React = require('react');

var TaskDetail = React.createClass({
    getInitialState: function() {
        return ({
            show: false
        })
    },
    click: function(){
        this.setState({
            show: !this.state.show
        })
    },
    unescapeHTML: function(a){  
        a = "" + a;
        return a.replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&").replace(/&quot;/g, '"').replace(/&apos;/g, "'");  
    },
    render: function(){
        var data = this.props.data
        var contact = this.props.contact.map(function(item,i){
            return <p key = {i}>认领用户：{item.username} 认领时间：{item.date}</p>
        })
        var dom = this.state.show == true? contact : []
        var string = this.state.show == true? "隐藏认领者联系方式" : "显示认领者联系方式"
        var html = this.unescapeHTML(data.content)
        return (
            <div className = "panel panel-success">
                <div className = "panel-heading">
                    {data.title}
                </div>
                <div className = "panel-body">
                    <p>¥{data.quote}</p>
                    <p dangerouslySetInnerHTML = {{__html: html}}></p>
                    <p>{data.number}</p>
                    <p><button type = "button" className = "btn btn-primary" onClick = {this.click}>{ string }</button></p>
                    {dom}
                </div>
            </div>
            )
    }
});

module.exports = TaskDetail;
