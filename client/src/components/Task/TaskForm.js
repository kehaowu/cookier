/** @jsx React.DOM */
var React = require('react');

var TaskForm = React.createClass({
    getInitialState: function() {
        return {
            submit: false,
            success: false
        }
    },
    postClick: function(){
      this.setState({
        submit: true
      })
      $.ajax({
          url: "/api/task",
          type:"POST",
          dataType:"json",
          headers:{
              Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
          },
          data : {
            'data':JSON.stringify({
                  title : this.refs.title.getDOMNode().value,
                  quote : this.refs.price.getDOMNode().value,
                  expiredate : this.refs.date.getDOMNode().value,
                  content : this.refs.content.getDOMNode().value
                })
          },
          success:function(data){
              if(data.status == "true"){
                this.setState({
                  success: true
                })
              } else {
                this.setState({
                  success: true,
                  submit: false
                })
              }
          }.bind(this),
          error: function(data){
                this.setState({
                  success: true,
                  submit: false
                })
          }.bind(this),
          async: true
      })
    },
    newClick: function(){
        this.setState({
          success: false,
          submit: false
        })
    },
    render: function(){
      var dom2 = <button type="button" className="btn btn-info" onClick = { this.newClick }>重新提交</button>
      if(this.state.submit == false && this.state.success == false){
          var dom = (
                <form>
                  <div className="form-group">
                    <label for="subject">标题</label>
                    <input type="text" className="form-control" ref = "title" defaultValue ="标题" />
                  </div>
                  <div className="form-group">
                    <label for="email">出价</label>
                    <input type="number" className="form-control" ref = "price" defaultValue ="100" />
                  </div>
                  <div className="form-group">
                    <label for="date">过期日期</label>
                    <input type="date" className="form-control" ref = "date"/>
                  </div>
                  <div className="form-group">
                    <label for="content">任务内容（支持Markdown语法）</label>
                    <textarea className="form-control" rows="5" ref = "content" />
                  </div>
                </form>
            )
          var dom2 = []
      }
      if (this.state.submit == true && this.state.success == true){
        var dom = <h4>提交成功！</h4>
      }
      if (this.state.submit == true && this.state.success == false){
        var dom = <h4>正在提交......</h4>
      }
      if (this.state.submit == false && this.state.success == true){
        var dom = <h4>提交失败！</h4>
      }
      return  (
        <div className="modal fade" id = "form" tabIndex="-1" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-body">
                  { dom }
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-default" data-dismiss="modal">关闭</button>
                  <button type="button" className="btn btn-danger">删除</button>
                  { dom2 }
                  <button type="button" className="btn btn-primary" onClick = { this.postClick }>提交</button>
                </div>
            </div>
          </div>
        </div> )
    }
});

module.exports = TaskForm;
