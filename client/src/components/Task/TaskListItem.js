/** @jsx React.DOM */
var React = require('react');

var TaskListItem = React.createClass({
    getInitialState: function() {
        return {
            claimed: false
        }
    },
    claimClick: function(){
        $.ajax({
            url:"/api/task/claim/" + this.props.item.taskid,
            type:"POST",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            success:function(data){
                if(data.status == "true"){
                    this.setState({
                        claimed: true
                    })
                }
            }.bind(this),
        })
    },
    render: function(){
        var item  = this.props.item
        var subdom = this.state.claimed == true ? <span className = "label label-info">已接</span> : <button className = "btn btn-success btn-xs" onClick = {this.claimClick}>接任务</button>
        var quote = item.quote == "-1" ? [] : <span className = "label label-primary">¥{item.quote}</span>
        return (
                <div className = "task-list-item" key = { this.props.key } data-id = {item.taskid} onClick = {this.props.itemClick.bind(this,item.taskid)}>
                    <p>{item.title}
                        
                    </p>
                    <p>
                        <small>发布日期：{item.postdate} 截止日期：{item.expiredate}</small>
                        { subdom }
                    </p>
                </div>
            )
    }
});

module.exports = TaskListItem;
