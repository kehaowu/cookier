/** @jsx React.DOM */
var React = require('react');
var TaskListItem = require('./TaskListItem.js');

var TaskList = React.createClass({
    getInitialState: function() {
        return {
            data: [],
            switcher: true
        }
    },
    loadData: function(){
        $.ajax({
            url:"/api/task",
            type:"GET",
            dataType:"json",
            success:function(data){
                if(data.status == "true"){
                    this.setState({
                        data: data.data
                    })
                }
            }.bind(this),
        })
    },
    claimClick: function(id){
        $.ajax({
            url:"/api/task/claim/" + id,
            type:"POST",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            success:function(data){
                if(data.status == "true"){
                }
            }.bind(this),
        })
    },
    componentWillMount: function() {
        this.loadData()
    },
    render: function(){
        var dom = this.state.data.map(function(item,i){
            return <TaskListItem item = {item} key = {i} token = {this.props.token} itemClick = {this.props.itemClick} />
        }.bind(this))
        return (
            <div className = "panel panel-success">
                <div>
                    <div className = "signinup-heading signinup-heading-active">所有任务</div>
                    <div className = "signinup-heading" onClick = { this.props.headerClick}>我认领的</div>
                </div>
                <div className = "panel-body">
                    { dom }
                </div>
            </div>
            )
    }
});

module.exports = TaskList;
