/** @jsx React.DOM */
var React = require('react');
var md5 = require('md5');

var ForgetPassword = React.createClass({
    getInitialState: function() {
        return {
            success : false,
            submit : false,
            click : false
        }
    },
    clickHandler: function() {
        this.setState({
            click: true
        })
        $.ajax({
            url:"/api/forgetpassword",
            type:"POST",
            data:{
                'email':this.refs.email.getDOMNode().value
            },
            dataType:"json",
            success: function(data){
                if(data.status == "true"){
                    this.setState({
                        success: true,
                        click: false
                    })
                } else {
                    this.setState({
                        submit: true,
                        click: false
                    })
                }
            }.bind(this),
            error: function(data){
                this.setState({
                    submit: true,
                    click: false
                })
            }.bind(this),
            async: true
        })
    },
    render: function(){
        if(this.state.success == true){
            var dom = <h4>修改密码链接已发送至您邮箱，请查收。</h4>
        } else {
            var dom = (
                    <form className="form-inline">
                      <div className="form-group">
                        <label htmlFor="newpassword">电子邮件</label>
                        <input type="email" className="form-control" ref="email" />
                      </div>
                      <button type="button" className="btn btn-default" onClick = {this.clickHandler}>找回密码</button>
                    </form>
                )
        }
        if(this.state.click == true){
            var dom = <h4>正在提交......</h4>
        }
        if(this.state.submit == true){
            var dom = <h4>提交不成功，请点击右下角反馈与我们联系</h4>
        }
        return (<div> { dom } </div>)
    }
})

module.exports = ForgetPassword;
