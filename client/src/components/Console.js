/** @jsx React.DOM */
var React = require('react');
var Display = require('./Console/Display.js');
var CommandLine = require('./Console/CommandLine.js');

var Console = React.createClass({
    getInitialState: function() {
        return({
            content: "R Version 3.2.2\n<h3 class = 'text-success'>R语言在线 <a href = 'http://www.rzaixian.com' target = '_blank'>www.Rzaixian.com</a></h3>\n",
            cmdSet:[],
            cmdIndex: -1,
            cmd: ""
        })
    },
    componentDidUpdate: function() {
       $("#console").scrollTop($("#console").prop("scrollHeight"))
    },
    enterCommand: function(event) {
        var cmd = event.target.value + String.fromCharCode(event.keyCode);
        var cmd1 = cmd.replace(/\s+/,"")
        if(event.keyCode == 38){
            if (this.state.cmdIndex < (this.state.cmdSet.length - 1)){
                this.setState({
                    cmdIndex: this.state.cmdIndex + 1
                })
            }
        } else if (event.keyCode == 40) {
            if (this.state.cmdIndex >= 0){
                this.setState({
                    cmdIndex: this.state.cmdIndex - 1
                }) 
            }
        } else if (event.keyCode == 13 & cmd1 != ""){
            var cmdSet = this.state.cmdSet
            cmdSet.unshift(cmd)
            $.ajax({
                url:"/api/cmd",
                data:{
                    'data':JSON.stringify({
                        "cmd":cmd,
                        'contentid':window.location.pathname.split("/")[2]
                    })
                },
                type:"POST",
                dataType:"json",
                headers:{
                    Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
                },
                success:function(data){
                    var tmp = data.data.replace(/\n$/,"")
                    tmp = tmp.search(/^Error/) == -1 ? tmp : "<span class='text-danger'>" + tmp + '</span>'
                    tmp = data.data == "" ? cmd.replace(/\r$/,"") : cmd + ("\n" + tmp)
                    var newline = this.state.cmdSet.length > 1 ? "\n> " + tmp : "> " + tmp
                    this.setState({
                        content: this.state.content + newline ,
                        cmdIndex: -1,
                        cmdSet: cmdSet
                    })
                }.bind(this),
                error: function(){
                    window.localStorage.removeItem('cookiertoken')
                    window.location.assign(window.location.pathname)
                },
                async:false
            })
        } 
    },
    render: function(){
        var cmd = this.state.cmdIndex == -1 ? this.state.cmd : this.state.cmdSet[this.state.cmdIndex]
        return (
            <pre id="console">
                <Display content = {this.state.content}/>
                <CommandLine
                    onKeyDown = {this.enterCommand}
                    cmd = {cmd} />
            </pre>
            )
    }
});

module.exports = Console;
