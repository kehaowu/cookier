/** @jsx React.DOM */
var React = require('react');

var CourseDetails = React.createClass({
    getInitialState: function(){
        return({
            data:[]
        })
    },
    componentWillMount: function() {
        var token = this.props.token == undefined ? "" : JSON.parse(window.localStorage.getItem(this.props.token)).token
        $.ajax({
            url: this.props.url,
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+ token
            },
            success:function(data){
                this.setState({
                    'data':data.data
                })
            }.bind(this),
            error: function(){
                window.localStorage.removeItem('cookiertoken')
                window.location.assign(window.location.pathname)
            },
            async:true
        });
    },
    render: function(){
        if(this.state.data.detail != undefined){
            var courseDom = (
                    <div className = "panel panel-primary">
                        <div className = "panel-heading">
                            {this.state.data.detail.title}
                        </div>
                        <div className = "panel-body">
                            {this.state.data.detail.description}
                        </div>
                    </div>
                )
            var contentDom = []
            for(var i =0; i<this.state.data.content.length; i++){
                var contentListDom = this.state.data.content[i].data.map(function(item,i){
                    style = {
                        marginLeft: "2em",
                        borderBottom: "1px solid #EEE"
                    }
                    var number = i + 1
                    var url = "/class/" + item.contentid
                    var color = item.status == 1 ? (<span className="glyphicon glyphicon-ok" ariaHidden="true"></span>) : ""
                    var key = item.title + i + item.contentid
                    if (this.props.blank == "_blank"){
                        return (
                                <p key = {key} style = {style}>{ number }. <a href = { url } target= "_blank">{item.title}</a> { color } </p>
                            )
                    }else{
                        return (
                                <p key = {key} style = {style}>{ number }. <a href = { url } >{item.title}</a> { color } </p>
                            )
                    }
                }.bind(this))
                var contentSubDom = (
                        <div className = "list-group-item" key = { i }>
                            <h2>{ this.state.data.content[i].title }</h2>
                            { contentListDom }
                        </div>
                    )
                contentDom.push(contentSubDom)
            }
        }else{
            courseDom = <h3>该门课程不存在！</h3>
        }
        if (this.props.courseDetails != "show"){
            courseDom = ""
        }
        return (
            <div>
                {courseDom}
                <div className = "list-group">
                    {contentDom}
                </div>
            </div>
            )
    }
})

module.exports = CourseDetails;
