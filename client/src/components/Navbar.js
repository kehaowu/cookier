/** @jsx React.DOM */
var React = require('react');

var Navbar = React.createClass({
    signOutClick: function() {
        localStorage.removeItem(this.props.token);
        window.location.assign(this.props.callback)
    },
    render: function() {
        var style = {
            marginRight: "1em"
        }
        if(window.localStorage.getItem(this.props.token) != undefined){
            var userinfo = JSON.parse(window.localStorage.getItem(this.props.token))
            now = new Date(),hour = now.getHours() 
            if(hour < 6){var hello = "凌晨好！"} 
            else if (hour < 9){var hello = "早上好！"} 
            else if (hour < 12){var hello = "上午好！"} 
            else if (hour < 14){var hello = "中午好！"} 
            else if (hour < 17){var hello = "下午好！"} 
            else if (hour < 19){var hello = "傍晚好！"} 
            else if (hour < 22){var hello = "晚上好！"} 
            else {var hello = "夜深了，早点休息！"} 

            var dom = <ul className="nav navbar-nav navbar-right"> <li><a>{ userinfo.username }，{hello}</a></li> <li style = {style} ><a onClick = { this.signOutClick }>退出账号</a></li></ul>
        }else{
            var dom = []
        }
        
        return(
                <header
                    className = "navbar navbar-inverse navbar-fixed-top"
                    role = "banner"
                >
                    <div
                        className = "navbar-header"
                    >

                        <button
                            className = "navbar-toggle collapsed"
                            type = "button"
                            data-toggle="collapse"
                            data-target="#navbar"
                            aria-controls="navbar"
                            aria-expanded="false"
                        >
                            <span className="sr-only">
                                导航栏
                            </span>
                            <span className="icon-bar">
                            </span>
                            <span className="icon-bar">
                            </span>
                            <span className="icon-bar">
                            </span>
                        </button>
                        <a href= { this.props.callback } className="navbar-brand"> <img src = "/indeximg/4.png" height = "100%" /> </a>
                    </div>
                    
                    <nav
                        id="navbar"
                        className="collapse navbar-collapse"
                    >
                        <ul className="nav navbar-nav">
                            <li> <a href="/">所有课程</a> </li>
                        </ul>
                        { dom }
                    </nav>
                </header>
            )
    }
});

module.exports = Navbar;