/** @jsx React.DOM */
var React = require('react');
var md5 = require('md5');

var ResetPassword = React.createClass({
    getInitialState: function() {
        return {
            success : false,
            submit : false,
            click: false
        }
    },
    clickHandler: function() {
        this.setState({
            click: true
        })
        $.ajax({
            url:"/api/passwdreset",
            type:"POST",
            data:{
                'password' : md5(this.refs.password.getDOMNode().value),
                'token': window.location.pathname.split("/")[2]
            },
            dataType:"json",
            success: function(data){
                if(data.status == "true"){
                    this.setState({
                        success: true,
                        click: false
                    })
                } else {
                    this.setState({
                        submit: true,
                        click: false
                    })
                }
            }.bind(this),
            error: function(data){
                this.setState({
                    submit: true,
                    click: false
                })
            }.bind(this),
            async: true
        })
    },
    render: function(){
        if(this.state.success == true){
            var dom = <h4>重置成功，请返回<a href='/'>首页</a>登录。</h4>
        } else {
            var dom = (
                    <form className="form-inline">
                      <div className="form-group">
                        <label htmlFor="newpassword">新密码</label>
                        <input type="password" className="form-control" ref="password"/>
                      </div>
                      <button type="button" className="btn btn-default" onClick = {this.clickHandler}>提交</button>
                    </form>
                )
        }
        if(this.state.click == true){
            var dom = <h4>正在提交......</h4>
        }
        if(this.state.submit == true){
            var dom = <h4>重置不成功，请点击右下角反馈与我们联系</h4>
        }
        return (<div> { dom } </div>)
    }
})

module.exports = ResetPassword;
