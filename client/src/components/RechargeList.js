/** @jsx React.DOM */
var React = require('react');

var RechargeList = React.createClass({
    getInitialState: function(){
        return ({
            data: []
        })
    },
    componentWillMount: function(id){
        $.ajax({
            url:"/admin/api/recharge/",
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            success:function(data){
                if(data.status = "true"){
                    this.setState({
                        'data': data.data
                    })
                }
            }.bind(this),
            error:function(){
            }
        })
    },
    render: function(){
        var dom = this.state.data.map(function(item,i){
            var className = item.userid != "" ? "success" : ""
            return (
                    <tr key = {i} className = {className}>
                        <td>{item.cardid}</td>
                        <td>{item.credit}</td>
                        <td>{item.expire_time}</td>
                        <td>{item.rechargetime}</td>
                        <td>{item.userid}</td>
                        <td>{item.createtime}</td>
                        <td>{item.adminid}</td>
                    </tr>
                )
        })
        return (
            <table className = "table table-striped table-hover">
                <tr>
                    <th>卡号</th>
                    <th>积分</th>
                    <th>过期时间</th>
                    <th>充值时间</th>
                    <th>充值用户</th>
                    <th>生成时间</th>
                    <th>管理员</th>
                </tr>
                {dom}
            </table>
            )
    }
});

module.exports = RechargeList;
