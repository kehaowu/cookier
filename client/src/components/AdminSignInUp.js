/** @jsx React.DOM */
var React = require('react');
var AdminSignIn = require('./AdminSignIn.js');
var AdminSignUp = require('./AdminSignUp.js');

var AdminSignInUp = React.createClass({
    getInitialState: function() {
        return {
            signIn : true,
            token : "cookiertoken",
        }
    },
    clickHandler: function() {
        this.setState({
            signIn : !this.state.signIn
        })
    },
    render: function(){
            var callback = this.props.callback == undefined ? "/admin" : this.props.callback
            var token = this.props.token == undefined ? this.state.token : this.props.token
            var signinurl = this.props.signinurl == undefined ? "/admin/api/signin" : this.props.signinurl
            var signupurl = this.props.signupurl == undefined ? "/admin/api/signup" : this.props.signupurl
            if (this.state.signIn == true){
                dom = (<div>
                                <AdminSignIn
                                    url = {signinurl}
                                    token = { token }
                                    onClick = {this.clickHandler} callback = {callback} />
                              </div> )
            }else{
                dom = (<div>
                                <AdminSignUp 
                                    url = {signupurl}
                                    token = { token }
                                    onClick = {this.clickHandler} callback = {callback} />
                                </div>)
            }
            return <div>{dom}</div>
    }
})

module.exports = AdminSignInUp;
