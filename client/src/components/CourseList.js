/** @jsx React.DOM */
var React = require('react');

var CourseList = React.createClass({
    getInitialState: function() {
        return({
            data: []
        })
    },
    componentWillMount: function() {
        $.ajax({
            url:"/api/course",
            type:"GET",
            dataType:"json",
            success:function(data){
                this.setState({
                    'data':data.data
                })
            }.bind(this),
            error:function(){
                window.localStorage.removeItem('cookiertoken')
            }.bind(this),
            async:true
        });
    },
    render: function() {
        var dom = this.state.data.map(function(item,i){
            var url = "/course/" + item.courseid
            var viewcount = Math.ceil( item.viewcount / 1 )
            return (
                <div className = "col-md-4" key = {i}>
                    <div className="panel panel-primary">
                        <div className = "panel-heading">
                            <a href={url} style = {{ color: "#FFF" }}> { item.title } </a>
                            <span className = "viewcount">  
                                <span className="glyphicon glyphicon-scale"></span>
                                { viewcount }℃
                            </span>
                        </div>
                        <div className = "panel-body">
                            <p style = {{ height : "12em", overflowY : "hidden" }}>
                                { item.description }
                            </p>
                            <a href={url}  className="btn btn-warning btn-block btn-sm">开始学习</a>
                        </div>
                    </div>
                </div>
                )
        })
        return (
            <div className = "row">
                {dom}
            </div>
            )
    }
})

module.exports = CourseList;
