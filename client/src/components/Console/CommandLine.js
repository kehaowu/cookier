/** @jsx React.DOM */
var React = require('react');

var CommandLine = React.createClass({
    getInitialState: function(){
        return({
            cmd: ""
        })
    },
    componentDidUpdate: function(){
        $("#console-input").width($("#console-command-line").width() -16-$("#console-prompt").width())
        $("#console-input").val("")
    },
    componentWillReceiveProps: function(nextProps) {
      this.setState({
        cmd: nextProps.cmd
      });
    },
    render: function(){
        var style = {
            backgroundColor: "transparent",
            margin: "0",
            padding: "0",
            border: "none",
            outline: "medium"
        }
        return (
                <div style = {{width:"100%"}} id="console-command-line">
                    <span id="console-prompt">{"> "}</span>
                    <input 
                        id="console-input"
                        style={style}
                        onChange = {this.onChange}
                        onKeyDown  = {this.props.onKeyDown}/>
                </div>
            )
    }
});

module.exports = CommandLine;
