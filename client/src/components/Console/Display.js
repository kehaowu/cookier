/** @jsx React.DOM */
var React = require('react');

var Display = React.createClass({
    componentDidMount: function(){
       $("#console-display").height($("#console-display").prop("scrollHeight"))
    },
    componentDidUpdate: function(){
       $("#console-display").height($("#console-display").prop("scrollHeight"))
    },
    render: function(){
        var style = {
            backgroundColor: "transparent",
            margin: "0",
            padding: "0",
            border: "none",
            width: "100%",
            outline: "medium",
            verticalAlign: "text-bottom",
            overflow: "hidden",
            resize: "none"
        }
        return (
                <pre id="console-display" readOnly={"readonly"} style={style} dangerouslySetInnerHTML = {{__html: this.props.content}}/>
            )
    }
});

module.exports = Display;
