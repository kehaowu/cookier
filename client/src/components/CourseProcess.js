/** @jsx React.DOM */
var React = require('react');

var CourseProcess = React.createClass({
    getInitialState: function(){
        return({
            data:[],
            pre:"没有了",
            preUrl:"",
            cur:"你猜",
            curUrl:"",
            nex:"没有了",
            nexUrl:""
        })
    },
    componentWillReceiveProps: function(nextProps) {
        var token = this.props.token == undefined ? "" : JSON.parse(window.localStorage.getItem(this.props.token)).token
        $.ajax({
            url:"/api/process/"+ nextProps.contentid,
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+token
            },
            success:function(data){
                this.setState({
                    pre:data.data.prev,
                    preUrl:data.data.prevurl,
                    cur:data.data.curr,
                    curUrl:data.data.currurl,
                    nex:data.data.next,
                    nexUrl:data.data.nexturl
                })
            }.bind(this),
            async:false
        })
    },
    componentWillMount: function() {
        var token = this.props.token == undefined ? "" : JSON.parse(window.localStorage.getItem(this.props.token)).token
        $.ajax({
            url:"/api/process/"+ this.props.contentid,
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+token
            },
            success:function(data){
                this.setState({
                    pre:data.data.prev,
                    preUrl:data.data.prevurl,
                    cur:data.data.curr,
                    curUrl:data.data.currurl,
                    nex:data.data.next,
                    nexUrl:data.data.nexturl
                })
            }.bind(this),
            async:false
        })
    },
    render: function(){
        var predom = this.state.preUrl == "" ? (
                <span>
                    <span className="glyphicon glyphicon-chevron-left" ariaHidden="true"></span>
                    { this.state.pre }
                </span>
            ) : (
                <a onClick = { this.props.updateClick } data-url = { this.state.preUrl }>
                    <span className="glyphicon glyphicon-chevron-left" ariaHidden="true"></span>
                    { this.state.pre }
                </a>
            )
        var nexdom = this.state.nexUrl == "" ? (
                <span>
                    <span className="glyphicon glyphicon-chevron-right" ariaHidden="true"></span>
                    { this.state.nex }
                </span>
            ) : (
                <a onClick = { this.props.updateClick } data-url = { this.state.nexUrl }>
                    <span className="glyphicon glyphicon-chevron-right" ariaHidden="true"></span>
                    { this.state.nex }
                </a>
            )
        return (
            <div className = "row course-process">
                <div className = "col-md-4 course-process-left">
                    { predom }
                </div>
                <div className = "col-md-4 course-process-center">
                    当前课程：{ this.state.cur }
                </div>
                <div className = "col-md-4 course-process-right">
                    { nexdom }
                </div>
            </div>
            )
    }
})

module.exports = CourseProcess;
