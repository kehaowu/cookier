/** @jsx React.DOM */
var React = require('react');

var CourseEditFrame = React.createClass({
    submitClick: function(){
        $.ajax({
            url:"/admin/api/course/"+this.props.data.courseid,
            type:"POST",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            data:{
                'data':JSON.stringify({
                    'title':this.refs.title.getDOMNode().value,
                    'description':this.refs.description.getDOMNode().value
                })
            },
            success:function(data){
                if(data.status = "true"){
                    alert("保存成功！")
                }
            },
            error:function(){
            }
        })
    },
    render: function() {
        return (
                <div className = {this.props.className}>
                    <p>课程标题：
                        <input
                                className = "frame-title"
                                defaultValue = { this.props.data.title }
                                ref = "title"/>
                    </p>
                    <textarea
                        className = "frame-content"
                        ref = "description"
                        defaultValue = { this.props.data.description }/>
                    <button 
                        ref = "submit"
                        className="btn btn-success btn-block"
                        onClick = {this.submitClick}>
                        保存
                    </button>
                </div>
            )
    }
})

module.exports = CourseEditFrame;
