/** @jsx React.DOM */
var React = require('react');
var ChapterListItem = require('./ChapterList/ChapterListItem.js');
var EditFrame = require('./ChapterList/EditFrame.js');

var ChapterList  = React.createClass({
    getInitialState: function() {
        return ({
            data:[],
            contentData:[],
            editableForExist:true,
            exist: true
        })
    },
    loadData: function(){
        $.ajax({
            url:this.props.url,
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            success: function(data){
                if(data.status == "true"){
                    data.data.push({
                        courseid: 0,
                        title: "创建新章节"
                    })
                    this.setState({
                        data: data.data
                    })                    
                }else{
                    this.setState({
                        exist: false
                    })
                }

            }.bind(this),
            error:function(data){
            }
        })
    },
    componentWillMount: function() {
        this.loadData()
    },
    deleteClick: function(id) {
        $.ajax({
            url:"/admin/api/chapter/"+id,
            type:"DELETE",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            success:function(data){
                this.loadData()
            }.bind(this),
            error:function(){
            }
        })
    },
    submitAdd: function(event){
        if(event.keyCode == 13){
            $.ajax({
                url:this.props.url,
                type:"POST",
                headers:{
                    Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
                },
                data:{
                    'data':JSON.stringify({
                        'title':event.target.value,
                        'rfile':" ",
                        'content':' '
                    })
                },
                dataType:"json",
                success:function(data){
                    if(data.status == "true"){
                        this.loadData()
                    }
                }.bind(this),
                error:function(data){
                }
            })
        }
    },
    contentClick: function(id) {
        $.ajax({
            url:"/admin/api/content/" + id,
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            success:function(data){
                this.setState({
                    'contentData':data.data[0]
                })
            }.bind(this),
            error:function(data){
            }.bind(this),
            async:true
        });
    },
    render: function(){
        if(this.state.exist == false){
            var dom = <h1>该课程不存在</h1>
        }else{
            var dom = this.state.data.map(function(item,i){
                var url = "/admin/api/chapter/" + item.courseid + "/content"
                var key = item.courseid + i + item.title
                return (<ChapterListItem
                                contentClick = {this.contentClick}
                                url = {url}
                                token = {this.props.token}
                                data = {item}
                                key = {key}
                                submitAdd = {this.submitAdd}
                                deleteClick = {this.deleteClick} />)
            }.bind(this))
        }

        var key = Date.now()
        return (
                <div className = "row">
                    <div className = "col-md-4">
                        {dom}
                    </div>
                    <div className = "col-md-8">
                        <EditFrame
                            key = {key}
                            token = {this.props.token}
                            data = {this.state.contentData} />
                    </div>
                </div>
            )
    }
})

module.exports = ChapterList;
