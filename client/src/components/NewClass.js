/** @jsx React.DOM */
var React = require('react');

var NewClass = React.createClass({
    getInitialState: function() {
        return({
            data: []
        })
    },
    componentWillMount: function() {
        var token = this.props.token == undefined ? "" : JSON.parse(window.localStorage.getItem(this.props.token)).token
        $.ajax({
            url:"/api/class/new/5",
            type:"GET",
            dataType:"json",
            success:function(data){
                this.setState({
                    'data':data.data
                })
            }.bind(this),
            error:function(){
                window.localStorage.removeItem('cookiertoken')
            }.bind(this),
            async:true
        });
    },
    render: function() {
        var dom = this.state.data.map(function(item,i){
            var url = "/class/" + item.contentid
            return (
                    <p className = "new-class-item" key = { i } >
                        <a href = { url } target = "_blank">{item.title}</a><br />
                        <small>
                            发布日期：{item.createtime}
                            <span className="glyphicon glyphicon-scale"></span>
                            { item.viewcount }℃
                        </small>
                    </p>
                )
        })
        return (
            <div>
                <h4>最新上线课程</h4>
                {dom}
            </div>
            )
    }
})

module.exports = NewClass;
