/** @jsx React.DOM */
var React = require('react');

var EditFrame = React.createClass({
    submitClick: function(){
        $.ajax({
            url:"/admin/api/content/"+this.props.data.contentid,
            type:"POST",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            data:{
                'data':JSON.stringify({
                    'title':this.refs.title.getDOMNode().value,
                    'weight':this.refs.weight.getDOMNode().value,
                    'rfile':"KEHAO",
                    'content':this.refs.content.getDOMNode().value
                })
            },
            success:function(data){
                if(data.status = "true"){
                    alert("保存成功！")
                }
            },
            error:function(){
            }
        })
    },
    render: function() {
        var url = "/class/" + this.props.data.contentid
        return (
                <div>
                    <p>链接:  
                        <a href={url} target="_blank"> 
                            { url } 
                        </a>
                    </p>
                    <p>内容标题：
                        <input
                                className = "frame-title"
                                defaultValue = { this.props.data.title }
                                ref = "title"/>
                    </p>
                    <textarea
                        className = "frame-content"
                        ref = "content"
                        defaultValue = { this.props.data.content }/>
                    <p>权重：
                        <input
                                className = "frame-weight"
                                defaultValue = { this.props.data.weight }
                                ref = "weight"/>
                    </p>
                    <button 
                        ref = "submit"
                        className="btn btn-success btn-block"
                        onClick = {this.submitClick}>
                        保存
                    </button>
                </div>
            )
    }
})

module.exports = EditFrame;
