/** @jsx React.DOM */
var React = require('react');
var DeleteButton = require('../DeleteButton.js');

var ContentListItem = React.createClass({
    getInitialState: function() {
        return ({
            editable: false
        })
    },
    cancelClick: function() {
        this.setState({
            editable: false
        })
    },
    addClick: function() {
        this.setState({
            editable: true
        })
    },
    render: function() {
        if(this.props.data.contentid != "0"){
            var dom = (
                <span
                    data-contentid = {this.props.data.contentid}
                    onClick = {this.props.contentClick.bind(null,this.props.data.contentid)}
                >
                    <span className = "content-list-item-main">
                        {this.props.data.title}
                    </span>
                    <span className = "content-list-item-button">
                        <DeleteButton 
                        onClick = {this.props.deleteClick.bind(null,this.props.data.contentid)} />
                    </span>
                </span>
                )
        }else{
            if(this.state.editable == true){
                var dom = (<span>
                    <input 
                        onKeyDown = { this.props.submitAdd }
                        className = "chapter-list-item-add-input" />
                    <button
                        className = "btn cancel-button"
                        onClick = {this.cancelClick}
                    >
                    取消
                    </button>
                </span>)
            }else{
                var dom = (
                                <span
                                    onClick = { this.addClick }
                                    className = ".chapter-list-item-add">
                                    {this.props.data.title}
                                </span>
                    )
            }
        }

        return (
                <p className = "content-list-item"> 
                    {dom}
                </p>
            )
    }
})

module.exports = ContentListItem;
