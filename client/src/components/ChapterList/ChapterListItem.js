/** @jsx React.DOM */
var React = require('react');
var ContentList = require('./ContentList.js');
var CollapseButton = require('../CollapseButton.js');
var DeleteButton = require('../DeleteButton.js');

var ChapterListItem = React.createClass({
    getInitialState: function() {
            return ({
                chapterData: [],
                data: [],
                collapse: true,
                editable: false,
                editableForExist: false
            })
    },
    loadData: function(collapseChange) {
        $.ajax({
            url:"/admin/api/chapter/" + this.props.data.courseid + "/content",
            type:"GET",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            success: function(data){
                var collapse = collapseChange ? !this.state.collapse : this.state.collapse
                data.data.push({
                    contentid: "0",
                    title: "创建新的内容"
                })
                this.setState({
                    data: data.data,
                    collapse: collapse
                })
            }.bind(this),
            error:function(data){
            }
        })
    },
    submitAddContent: function(event){
        var url = "/admin/api/chapter/" + this.props.data.courseid + "/content"
        if(event.keyCode == 13){
            $.ajax({
                url: url,
                type:"POST",
                headers:{
                    Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
                },
                data:{
                    'data':JSON.stringify({
                        'title':event.target.value,
                        'rfile':" ",
                        'content':' '
                    })
                },
                dataType:"json",
                success:function(data){
                    if(data.status == "true"){
                        this.loadData()
                    }
                }.bind(this),
                error:function(data){
                }
            })
        }
    },
    collapseClick: function() {
        if (this.state.collapse == false){
            this.setState({
                data:[],
                collapse:!this.state.collapse
            })
        }else{
            this.loadData(collapseChange=true)
        }
    },
    deleteClick: function(id) {
        $.ajax({
            url:"/admin/api/content/"+id,
            type:"DELETE",
            dataType:"json",
            headers:{
                Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
            },
            success:function(data){
                this.loadData(collapseChange=false)
            }.bind(this),
            error:function(){
            }
        })
    },
    addClick: function() {
        this.setState({
            editable: true
        })
    },
    cancelClick: function() {
        this.setState({
            editable: false
        })
    },
    editClick: function() {
        this.setState({
            editableForExist: true
        })
    },
    cancelEditClick: function(){
        this.setState({
            editableForExist: false
        })
    },
    submitEdit: function(event) {
        var value = event.target.value
        if(event.keyCode == 13){
            $.ajax({
                url:"/admin/api/chapter/"+event.target.getAttribute('data-id'),
                type:"POST",
                headers:{
                    Authorization : "Token "+JSON.parse(window.localStorage.getItem(this.props.token)).token
                },
                data:{
                    'data':JSON.stringify({
                        'title':event.target.value,
                        'description':" "
                    })
                },
                dataType:"json",
                success:function(data){
                    if(data.status == "true"){
                        this.setState({
                            editableForExist: false,
                            chapterData: {
                                title: value,
                                description: ""
                            }
                        })
                    }
                }.bind(this),
                error:function(data){
                }
            })
        }
    },
    componentWillMount: function() {
        this.setState({
            chapterData: this.props.data
        })
    },
    render: function(){
        var style = this.state.collapse ? "none" : "inline"
        if (this.state.chapterData.courseid != 0){
            if(this.state.editableForExist == false){
                var dom = (
                            <span>
                                <span className = "chapter-list-item-main" onClick = {this.editClick}>
                                    {this.state.chapterData.title}
                                </span>
                                <span className = "chapter-list-item-button">
                                    <DeleteButton 
                                        onClick = { this.props.deleteClick.bind(null,this.state.chapterData.courseid) }/>
                                    <CollapseButton
                                        onClick = {this.collapseClick} 
                                        collapse = {this.state.collapse} />
                                </span>
                            </span>
                    )
            }else{
                var dom = (
                            <span>
                                    <input
                                        data-id = {this.state.chapterData.courseid}
                                        onKeyDown = { this.submitEdit }
                                        defaultValue = { this.state.chapterData.title}
                                        className = "chapter-list-item-add-input" />
                                    <button
                                        className = "btn cancel-button"
                                        onClick = {this.cancelEditClick}
                                    >
                                    取消
                                    </button>
                            </span>
                    )
            }
        }else{
            if( this.state.editable == true ){
                var dom = (
                                <span>
                                    <input 
                                        onKeyDown = { this.props.submitAdd }
                                        className = "chapter-list-item-add-input" />
                                    <button
                                        className = "btn cancel-button"
                                        onClick = {this.cancelClick}
                                    >
                                    取消
                                    </button>
                                </span>
                    )
            }else{
                var dom = (
                                <span
                                    onClick = { this.addClick }
                                    className = ".chapter-list-item-add">
                                    {this.state.chapterData.title}
                                </span>
                    )
            }
        }
        return (
                <div>
                    <p
                        className = "chapter-list-item"
                    >
                        { dom }
                    </p>
                    <ContentList
                        style = {style}
                        contentClick = {this.props.contentClick}
                        submitAdd = {this.submitAddContent}
                        deleteClick = {this.deleteClick}
                        courseid = {this.state.chapterData.courseid}
                        data = {this.state.data} />
                </div>
            )
    }
})

module.exports = ChapterListItem;
