/** @jsx React.DOM */
var React = require('react');
var ContentListItem = require('./ContentListItem.js');

var ContentList  = React.createClass({
    getInitialState: function() {
        return ({
            data:[]
        })
    },
    render: function(){
        var dom = this.props.data.map(function(item,i){
            var key = item.contentid + i
            return(
                    <ContentListItem
                        contentClick = {this.props.contentClick}
                        submitAdd = { this.props.submitAdd}
                        data = {item}
                        key = {key}
                        deleteClick = {this.props.deleteClick} />
                )
        }.bind(this))
        var style = {
            display: this.props.style
        }
        return (
                <div style = {style}>
                    {dom}
                </div>
            )
    }
})

module.exports = ContentList;