/** @jsx React.DOM */
var React = require('react');
var md5 = require('md5');

var SignUp = React.createClass({
    loginClick: function(event){
        $.ajax({
            url:this.props.url,
            type:"POST",
            dataType:"json",
            data : {
                    email : this.refs.email.getDOMNode().value,
                    password : md5(this.refs.password.getDOMNode().value)
            },
            success:function(data){
                localStorage.setItem(this.props.token, JSON.stringify(data));
                if (localStorage.getItem(this.props.token) != undefined){
                    window.location.assign(window.location.pathname)
                } else {
                    alert("注册失败！")
                }
            }.bind(this),
            error: function(data){
                 alert("注册失败！")
            }.bind(this)
        })
    },
    render: function() {
        var aStyle = {color : "#FF0000"}
        return (
            <div className = "panel panel-success">
                <div>
                    <div className = "signinup-heading" onClick = {this.props.onClick}>登录</div>
                    <div className = "signinup-heading signinup-heading-active">注册</div>
                </div>
                <div className = "panel-body">
                    <form>
                        <div className = "form-group">
                            <label> 电子邮件 </label>
                            <input 
                                ref = "email"
                                type="email"
                                className = "form-control"
                            >
                            </input>
                        </div>
                        <div className = "form-group">
                            <label> 密码 </label><a href="/forgetpassword" className="text-warning" style={{ paddingLeft: "5px" }}>忘记密码?</a>
                            <input
                                ref = "password"
                                type="password"
                                className = "form-control"
                            >
                            </input>
                        </div>
                        <div className = "form-group">
                            <input 
                                type="button" 
                                className = "btn btn-warning btn-block"
                                onClick = {this.loginClick}
                                value = "注册"
                            >
                            </input>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
    
});
    
module.exports = SignUp;