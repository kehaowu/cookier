/** @jsx React.DOM */
var React = require('react');
var AdminSignInUp = require('./components/AdminSignInUp.js');
var Navbar = require("./components/Navbar.js");
var ChapterList = require("./components/ChapterList.js");

var App = React.createClass({
    getInitialState: function() {
        return {
            signIn : true,
            token : "admintoken",
            data: []
        }
    },
    clickHandler: function() {
        this.setState({
            signIn : !this.state.signIn
        })
    },
    componentWillMount:function(){
        var token = this.state.token
        if(localStorage.getItem(token) != undefined){
            var x =  window.location.pathname.split("/")
            x.splice(2,0,"api")
            var url = x.join("/")
            $.ajax({
                url:url,
                type:"GET",
                dataType:"json",
                headers:{
                    Authorization : "Token "+JSON.parse(window.localStorage.getItem(token)).token
                },
                success:function(data){
                    this.setState({
                        'data':data.data[0]
                    })
                }.bind(this),
                error:function(){
                }.bind(this),
                async:true
            });
        }
    },
    render: function() {
        var token = this.state.token
        if (localStorage.getItem(token) == undefined)
        {
            var dom = (<div
                    className = "col-md-3">
                    <AdminSignInUp 
                        signinurl = "/admin/api/signin"
                        signupurl = "/admin/api/signup"
                        token = {token}
                        callback = "/admin"/>
                </div>)
        }else{
            var x =  window.location.pathname.split("/")
            x.splice(2,0,"api")
            var url = x.join("/")
            url += "/chapter"
            var dom = (<div className = "col-md-12">
                    <h1>课程名称： {this.state.data.title}</h1>
                    <hr/>
                    <ChapterList url = { url } token = { token } />
                </div>)
        }
        
        return (
                <div>
                    <Navbar
                        logo = "数据学院管理员后台"
                        token = {token}
                        callback = "/admin" />
                    <div className = "cookier">
                    { dom }
                    </div>
                </div>
            )
    }
});

React.render(
      <App />,
      document.getElementById('app')
);
