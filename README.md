# cookieR
An online learning system for R users.
![demo.png](https://bitbucket.org/repo/KK5A8g/images/181047872-demo.png)

```shell
# Run R demon process manager in terminal 1.
[conda@localhost cookieR]$ python Rmanager.py
Empty redis data.
60010
localhost
Run at port 60010
Welcome to server!
Check any port available or not.
Check any port available or not.
2a21fff2-69bc-11e5-9317-080027e20e36 60024
Loading R/test.RData
[1] "/home/conda/cookieR"
Listening... at port 60024
[1] "[1] 1"
Listening... at port 60024
```

```shell
# Run in terminal 2.
## Create a R demon process.
[conda@localhost cookieR]$ python Rclient.py
Check any port available or not.
Port 60024
python Rclient.py 2a21fff2-69bc-11e5-9317-080027e20e36 60024

## Then send a command to R demon process.
[conda@localhost cookieR]$ python Rclient.py 2a21fff2-69bc-11e5-9317-080027e20e36 60024 "print(1)"
[1] 1
## Every command has been sent to R demon process.
```

definition of degree

** x ** is count of viewer.

** g ** is good count

** b ** is bad count

** d ** is how many days it has been created

degree = x ^ (3/2) + x ^ (1/2) * g ^2 - b ^ 2 + d + 1
