#!/usr/bin/env python
#coding=utf-8
from .._base import *
import uuid, time, hashlib, random, base64

class TokenMixIn:
    def generate_token(self, user_id):
        rand = hashlib.md5((
            str(time.time()) +
            str(random.uniform(0,1)) +
            str(user_id).zfill(8)
        ).encode()).digest()
        token = base64.b64encode(uuid.uuid4().bytes + rand[:64]).decode()
        self.db.execute(
            'INSERT INTO token (user_id,token) VALUES(%s,%s)',
            user_id, token)
        return token

class SignUpHandler(TokenMixIn, BaseHandler):
    def post(self):
        email = self.get_param('email')
        username = email
        if email.find('@') < 0:
            self.write_error_data(422, {
                'code': 200,
                'message': 'Invalid email.',
            })
        exists = self.db.query('SELECT email FROM user WHERE email = %s', email)
        if exists:
            self.write_error_data(422, {
                'code': 201,
                'message': 'Email has been signed up.',
            })
        pwd = self.get_param('password')
        if not pwd or len(pwd) < 6:
            self.write_error_data(422, {
                'code': 300,
                'message': 'Invalid password.',
            })
        try:
            user_id = self.db.execute(
                'INSERT INTO user (username,password,email) '
                'VALUES (%s,PASSWORD(%s),%s)',
                username,pwd,email)
            assert user_id
        except:
            print 4
            self.write_error_data(500)
        token = self.generate_token(user_id)
        self.write_data({
            'id': user_id,
            'token': token,
            'username': username,
        })

class SignInHandler(TokenMixIn, BaseHandler):
    def post(self):
        email = self.get_param('email')
        password = self.get_param('password')
        if not email or not password or len(password) < 6 or not email.find('@') > 0:
            self.write_error_data(422, 'Invalid email or password.')
        sql = ['SELECT id,username FROM user WHERE status=1 AND password=PASSWORD(%s)']
        args = [password]
        sql.append('email=%s')
        args.append(email)
        res = self.db.get(' AND '.join(sql), *args)
        if not res:
            self.write_error_data(401, 'Invalid email or password.')
        user_id = res['id']
        username = res['username']
        token = self.generate_token(user_id)
        self.write_data({
            'id': user_id,
            'token': token,
            'username': username,
        })

class UserInfoHandler(BaseHandler):
    @require_token
    def get(self):
        self.write_data(self.user)

class SignOutHandler(BaseHandler):
    @require_token
    def delete(self):
        self.db.execute('DELETE FROM token WHERE token=%s', self.user['token'])
        self.write_data(None)

from lib.apis.mail import mail
# /api/forgetpasswd
class ForgetHandler(TokenMixIn, BaseHandler):
    def post(self):
        email = self.get_argument("email")
        status = 'false'
        if email.find('@') < 0:
            self.write_error_data(422, {
                'code': 200,
                'message': 'Invalid email.',
            })
        exists = self.db.query('SELECT email FROM user WHERE email = %s', email)
        if not exists:
            self.write_error_data(422, {
                'code': 201,
                'message': 'Email has been signed up.',
            })
        else:
            rand = hashlib.md5((
                str(time.time()) +
                str(random.uniform(0,1)) +
                str(email).zfill(8)
            ).encode()).digest()
            token = hashlib.md5(uuid.uuid4().bytes + rand[:64]).hexdigest()
            opr = self.db.execute("INSERT INTO passwdreset (email, token) VALUES (%s, %s)", email, token)
            m = mail()
            m.set_subject("R语言在线：重设密码")
            body = "<p><a href='http://www.rzaixian.com/passwdreset/"
            import sys
            body = body + token
            body = body + u'\'>点此链接</a>重设密码，一次有效。系统自动发送，请勿回复。</p>'
            sys.stderr.write(token)
            m.set_body(body)
            m.sendmail(email)
            status = "true"
        self.write_data({
                'status': status
            })

# /api/passwdreset
class PasswdResetHandler(BaseHandler):
    def post(self):
        status = "false"
        password = self.get_param('password')
        token = self.get_param('token')
        if not password or len(password) < 6 :
            self.write_error_data(422, 'Invalid password.')
        opr = self.db.get("SELECT email FROM passwdreset WHERE token = %s AND status = 0 AND expiretime>CURRENT_TIMESTAMP", token)
        if not opr:
            self.write_error_data(401, 'Invalid token.')
            return
        email = opr['email']
        opr = self.db.execute("UPDATE passwdreset SET status = 1 WHERE token = %s", token)
        opr = self.db.execute("UPDATE user SET password = PASSWORD(%s) WHERE email = %s", password, email)
        status = "true"
        self.write_data({
            'status': status
        })

