#coding=utf-8
from lib._base import *
from lib.apis.mail import mail

# /api/forgetpasswd
class ForgetHandler(BaseHandler):
    def post(self):
        email = self.jsonload(self.get_argument("email"))
