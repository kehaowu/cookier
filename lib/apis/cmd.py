# coding=utf-8
import json,time,socket, sys
from lib._base import *
from markdown import markdown

class cmdHandler(BaseHandler):
    @require_token
    def post(self):
        data = json.loads(self.get_argument("data"))
        cmd = data['cmd']
        contentid = data['contentid']
        userid = str(self.user['id'])
        param = self.gettokenandrfile(userid,contentid)
        token = param['token']
        rfile = param['rfile']
        try:
            print cmd
        except:
            cmd = '''cat("aha！好像出了点问题，换个命令试试！")'''
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if socket.gethostname() == "localhost.localdomain":
            client_socket.connect(("localhost", 60011))
        else:
            client_socket.connect(("rserver_1", 60011))
        data = json.dumps({
            'token':token,
            'cmd':cmd,
            'rfile':param['rfile']
            }).encode('utf-8')
        client_socket.sendall(data+"\n")
        res = client_socket.recv(5000)
        client_socket.close()
        self.write(
            json.dumps({
                    'data':res
                }).encode('utf-8')
            )


class initHandler(BaseHandler):
    @require_token
    def get(self,contentid):
        userid = str(self.user['id'])
        param = self.gettokenandrfile(userid,contentid)
        token = param['token']
        rfile = param['rfile']
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if socket.gethostname() == "localhost.localdomain":
            client_socket.connect(("localhost", 60011))
        else:
            client_socket.connect(("rserver_1", 60011))
        data = json.dumps({
            'token':token,
            'cmd':"print('OK')",
            'rfile':param['rfile']
            }).encode('utf-8')
        client_socket.sendall(data+"\n")
        res = client_socket.recv(5000)
        client_socket.close()
        content = markdown(param['content'])
        self.write(
            json.dumps({
                    'data':content,
                    'viewcount':param['viewcount'],
                    'good':param['good'],
                    'bad':param['bad'],
                    'title':param['title']
                }).encode('utf-8')
            )
