# coding=utf-8
from lib._base import *
from lib.apis.mail import mail
import re, sys

#/api/course 
#GET ALL course; POST Create new course
class CourseHandler(BaseHandler):
    def get(self):
        try:
            info = self.db.query("SELECT C.courseid,C.title,C.description, \
                CAST(CEILING(POWER(SUM(A.viewcount),1.5) + \
                    SUM(A.good) * POWER(SUM(A.viewcount), 0.5) + \
                    - POWER(SUM(A.bad),2)+ 1 )\
                AS CHAR(10)) AS \
                viewcount FROM content A JOIN course B ON B.courseid = A.chapterid \
                JOIN course C ON B.parentid = C.courseid \
                WHERE A.status = 1 AND B.status = 1 \
                GROUP BY C.id ORDER BY viewcount DESC;")
            info2 = sorted(info, key = lambda x:x['viewcount'], reverse = True)
            status = "true"
        except:
            info = ""
            status = "false"
        self.write_data({
                'status':status,
                'data':info2
            })

#/api/class/new/(\d+)
class NewClassHandler(BaseHandler):
    def get(self,count):
        data = " "
        status = "false"
        data = self.db.query("SELECT contentid,title, \
            CAST(CEILING(POWER(viewcount,1.5) + \
                    good* POWER(viewcount, 0.5) + \
                    - POWER(bad,2)+ 1 )\
                AS CHAR(10)) AS \
                viewcount, \
            DATE_FORMAT(createtime,'%%c-%%d') AS createtime \
            FROM content WHERE status = 1 ORDER BY createtime DESC LIMIT 5;")
        status = "true"
        self.write_data({
                'status':status,
                'data':data
            })

#/api/course/(\w+)
#GET ALL course; POST Create new course
class CourseByIdHandler(BaseHandler):
    @require_token
    def get(self,courseid):
        try:
            res = self.db.get("SELECT courseid,title,description FROM course WHERE parentid = 0 AND courseid = %s", courseid)
            userid =  self.user['id']
            if res:
                info = self.db.query("SELECT A.title AS chaptertitle,A.courseid,B.title,B.contentid,C.status FROM course A JOIN content B  \
                    ON A.courseid = B.chapterid LEFT JOIN (SELECT * FROM record WHERE userid = %s) C ON B.contentid = C.contentid \
                    WHERE A.parentid=%s AND A.status=1 AND B.status=1 ORDER BY B.weight ASC;"\
                    ,userid,courseid)
                info1 = []
                infodict = None
                for item in info:
                    print ">>>"*2,item
                    if item.status == None:
                        item.status = 0
                    if infodict:
                        if infodict['title'] == item.chaptertitle:
                            infodict['data'].append({'title':item.title,'contentid':item.contentid,'status':item.status})
                        else:
                            print "-"*10
                            print infodict
                            info1.append(infodict)
                            infodict = {
                                'title':item.chaptertitle,
                                'data':[{'title':item.title,'contentid':item.contentid,'status':item.status}],
                                'status': 0
                            }

                    else:
                        infodict = {
                            'title':item.chaptertitle,
                            'data':[{'title':item.title,'contentid':item.contentid,'status':item.status}]
                        }
                if infodict:
                    print "-"*10
                    print infodict
                    info1.append(infodict)
                else:
                    info1 = []
                data = {
                    "detail":res,
                    "content":info1
                }
                status = "true"
            else:
                data = " "
                status = "false"
        except Exception,e:
            print e
            data = " "
            status = "false"
        self.write_data({
                'status':status,
                'data':data
            })

#/api/content/(\w+)/(\w+)
class ContentEvaluationHandler(BaseHandler):
    @require_token
    def post(self, contentid, evaluation):
        userid = self.user['id']
        status = "false"
        res = self.db.get("SELECT contentid FROM content \
            WHERE contentid = %s AND status = 1", contentid)
        print >> sys.stderr, 'My error 1:', res
        if res:
            res = self.db.get("SELECT * FROM evaluation\
                WHERE contentid = %s AND userid = %s",\
                contentid, userid)
            print >> sys.stderr, 'My error 2:', res
            if not res:
                sql = ["UPDATE content SET "]
                sql2 = ['INSERT INTO evaluation (contentid, userid,']
                args2 = [contentid, userid, 1]
                if evaluation == "good":
                    sql.append("good = good + 1")
                    sql2.append("good")
                elif evaluation == "bad":
                    sql.append("bad = bad + 1")
                    sql2.append("bad")
                sql2.append(") VALUES (%s,%s,%s)")
                sql.append("WHERE contentid = %s AND status = 1")
                args = [contentid]
                opr = self.db.execute(" ".join(sql), *args)
                opr = self.db.execute(" ".join(sql2), *args2)
                status = "true"
        self.write_data({
                'status': status
            })

#/api/content/(\w+)/course
#GET ALL course; POST Create new course
class CourseByContentIdHandler(BaseHandler):
    @require_token
    def get(self,contentid):
        try:
            res = self.db.get("select courseid,title,description from course \
                where courseid = (select parentid from course \
                    where courseid = (select chapterid from content where contentid=%s and status =1 )\
                    and status = 1) and status=1;",contentid)
            if res:
                info = self.db.query("SELECT A.title AS chaptertitle,A.courseid,B.title,B.contentid,C.status FROM course A JOIN content B  \
                    ON A.courseid = B.chapterid LEFT JOIN (SELECT * FROM record WHERE userid = %s) C ON B.contentid = C.contentid \
                    WHERE A.parentid=%s AND A.status=1 AND B.status=1 ORDER BY A.courseid ASC;",self.user['id'],res.courseid)
                info1 = []
                infodict = None
                for item in info:
                    if item.status == None:
                        item.status = 0
                    if infodict:
                        if infodict['title'] == item.chaptertitle:
                            infodict['data'].append({'title':item.title,'contentid':item.contentid,'status':item.status})
                        else:
                            info1.append(infodict)
                            infodict = {
                                'title':item.chaptertitle,
                                'data':[{'title':item.title,'contentid':item.contentid,'status':item.status}]
                            }
                    else:
                        infodict = {
                            'title':item.chaptertitle,
                            'data':[{'title':item.title,'contentid':item.contentid,'status':item.status}]
                        }
                if infodict:
                    info1.append(infodict)
                else:
                    info1 = []
                data = {
                    "detail":res,
                    "content":info1
                }
                status = "true"
            else:
                data = " "
                status = "false"
        except:
            data = " "
            status = "false"
        self.write_data({
                'status':status,
                'data':data
            })

#/api/record/(\w+)
#POST update user record
class RecordHandler(BaseHandler):
    @require_token
    def post(self,contentid,finished):
        userid = self.user['id']
        query = self.db.get("SELECT status FROM record WHERE contentid = %s AND userid = %s",contentid,userid)
        print "--------------"
        print query
        if query == None:
            operation = self.db.get("SELECT contentid FROM content WHERE contentid = %s AND status = 1",contentid)
            print operation
            if operation == None:
                status = "false"
            else:
                operation = self.db.execute("INSERT INTO record (userid,contentid,createtime,updatetime) \
                VALUES (%s,%s,now(),now());",userid,contentid)
                status = "true"
        elif finished == "1":
            try:
                self.db.execute("UPDATE record SET status = 1 WHERE contentid = %s AND userid = %s", contentid, userid)
                status = "true"
            except:
                status = "false"
        else:
            status = "true"
        print status
        self.write_data({
                'status':status
            })

#/api/recharge/(\w+)
#POST recharge
class RechargeHandler(BaseHandler):
    @require_token
    def post(self):
        userid = self.user['id']
        data = self.jsonload(self.get_argument("data"))
        query = self.db.get("SELECT credit FROM rechargecard \
            WHERE cardid = %s AND userid is NULL \
            AND expire_time >= now() ",data['card'])
        print query
        print data['card']
        if query:
            try:
                self.db.execute("UPDATE rechargecard SET \
                    rechargetime = now(), userid = %s WHERE cardid = %s", userid, data['card'])
                self.db.execute("UPDATE user SET credit = credit + %s \
                    WHERE id = %s", query['credit'],userid)
                status = "true"
            except:
                status = "false"
        else:
            status = "false"
        self.write_data({
                'status':status
            })

#/api/process/<contentid>
#GET get previous class and next class url
class ProcessHandler(BaseHandler):
    @require_token
    def get(self,contentid):
        status = "false"
        info = ""
        query = self.db.get("SELECT title,contentid FROM content WHERE contentid = %s", contentid)
        if query:
            try:
                info = self.db.query("SELECT A.title,A.contentid FROM content A \
                    JOIN course B ON A.chapterid = B.courseid WHERE \
                    (SELECT C.parentid from course C JOIN content D ON \
                        C.courseid = D.chapterid WHERE D.contentid = %s) \
                    AND A.status = 1 ORDER BY A.weight", contentid)
                print query
                index = info.index(query)
                if index == 0:
                    prev = "没有了"
                    prevurl = ""
                else:
                    prev = info[index - 1]['title']
                    prevurl = str(info[index - 1]['contentid'])
                if index == (len(info) - 1):
                    next = "没有了"
                    nexturl = ""
                else:
                    next = info[index + 1]['title']
                    nexturl = str(info[index + 1]['contentid'])
                curr = query['title']
                currurl = str(query['contentid'])
                info = {
                    'prev':prev,
                    'prevurl':prevurl,
                    'curr':curr,
                    'currurl':currurl,
                    'next':next,
                    'nexturl':nexturl
                }
                status = "true"
            except:
                status = "false"
        self.write_data({
                'status':status,
                'data':info
            })

#/api/feedback
#POST send feedback to us
class FeedbackHandler(BaseHandler):
    def post(self):
        status = "false"
        data = self.jsonload(self.get_argument("data"))
        print data
        m = mail()
        m.set_subject(data['subject'])
        m.set_body("<b>Contact:" + data['email'] + "</b>" + data['content'])
        m.sendmail("kehao.wu@rzaixian.com")
        m.sendmail("kai.yu@rzaixian.com")
        status = "true"
        self.write_data({
                'status':status
            })

