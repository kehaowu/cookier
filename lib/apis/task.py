#coding=utf-8
from lib._base import *
from lib.apis.mail import mail
from markdown import markdown
import re, sys


#/api/task 
#GET ALL task; POST Create new course
class TaskHandler(BaseHandler):
    def get(self):
        try:
            userid = self.user['id']
        except:
            userid = 0
        info = ""
        status = "false"
        info = self.db.query("SELECT A.taskid, A.title, A.quote, A.content, \
            DATE_FORMAT(A.postdate, '%%Y年%%m月%%d日') AS postdate, \
            DATE_FORMAT(A.editdate, '%%Y年%%m月%%d日') AS editdate, \
            DATE_FORMAT(A.expiredate, '%%Y年%%m月%%d日') AS expiredate, \
            B.username FROM task A JOIN user B ON \
            A.userid = B.id WHERE A.status = 1 ORDER BY A.postdate DESC LIMIT 20;")
        status = "true"
        self.write_data({
                'status':status,
                'data':info,
                'userid':userid
            })
    @require_token
    def post(self):
        userid = self.user['id']
        data = self.jsonload(self.get_argument("data"))
        sys.stderr.write(data['quote'])
        opr = self.db.execute("INSERT INTO task (userid,title,quote,\
            content,postdate,editdate,expiredate) VALUES (%s,%s,%s,\
            %s,now(),now(),%s)", userid, data['title'],data['quote'],data['content'],data['expiredate'])
        if opr:
            status = "true"
        else:
            status = "false"
        self.write_data({
                'status':status
            })

#/api/task/claim/<id>
#POST claim task; DEL Cancel task claim
class ClaimTaskHandler(BaseHandler):
    @require_token
    def post(self,taskid):
        status = "false"
        userid = self.user['id']
        opr = self.db.get("SELECT taskid FROM task WHERE taskid = %s AND status = 1",taskid)
        if opr:
            opr = self.db.get("SELECT userid FROM claim WHERE taskid = %s AND userid = %s", taskid, userid)
            if not opr:
                opr = self.db.execute("INSERT INTO claim (taskid,userid,quote,date) \
                    VALUES(%s,%s,-1,now())", taskid, userid)
                status = "true"
        self.write_data({
                'status':status
            })
    @require_token
    def delete(self,taskid):
        status = "false"
        userid = self.user['id']
        data = self.jsonload(self.get_argument("data"))
        opr = self.db.get("SELECT taskid FROM task WHERE taskid = %s AND status = 1",taskid)
        if opr:
            opr = self.db.execute("UPDATE claim SET status = 0 WHERE taskid = %s\
                AND userid = %s AND date = (SELECT MAX(date) FROM claim \
                    WHERE taskid = %s AND userid = %s)", taskid, userid, taskid, userid,)
            status = "true"
        self.write_data({
                'status':status
            })

#/api/task/<taskid>
#POST modify; GET fetch details; DEL delete
class TaskDetailsHandler(BaseHandler):
    @require_token
    def get(self,taskid):
        res = self.db.get("SELECT count(C.taskid) AS number,A.taskid, A.title, A.quote, A.content, \
                DATE_FORMAT(A.postdate, '%%Y年%%m月%%d日') AS postdate, \
                DATE_FORMAT(A.editdate, '%%Y年%%m月%%d日') AS editdate, \
                DATE_FORMAT(A.expiredate, '%%Y年%%m月%%d日') AS expiredate, \
                B.username FROM task A JOIN user B ON A.userid = B.id\
                JOIN claim C ON C.taskid = A.taskid WHERE A.status = 1 AND A.taskid = %s;", taskid)
        res['content'] = markdown(res['content'])
        res3 = self.db.get("SELECT userid FROM task WHERE status = 1 AND taskid = %s", taskid)
        res2 = []
        if res3:
            if res3['userid'] == self.user['id']:
                res2 = self.db.query("SELECT A.username, \
                    DATE_FORMAT(B.date, '%%Y年%%m月%%d日') AS date FROM claim B JOIN user A ON B.userid = A.id \
                    WHERE B.taskid = %s", taskid)
        if res:
            status = "true"
        else:
            status = "false"
        self.write_data({
                'status':status,
                'data':res,
                'contact': res2
            })
    @require_token
    def post(self,taskid):
        status = "false"
        userid = self.user['id']
        data = self.jsonload(self.get_argument("data"))
        res = self.db.get("SELECT taskid FROM task WHERE userid = %s AND taskid = %s\
         AND status = 1", userid, taskid)
        if res:
            res = self.db.execute("UPDATE task SET title = %s, \
                quote = %s, content = %s, expiredate = %s \
                WHERE userid = %s AND taskid = %s AND \
                status = 1", title, quote, expiredate, userid, taskid)
            status = "true"
        else:
            status = "false"
        self.write_data({
                'status':status
            })
    @require_token
    def delete(self,taskid):
        status = "false"
        userid = self.user['id']
        res = self.db.get("SELECT taskid FROM task WHERE userid = %s AND taskid = %s\
         AND status = 1", userid, taskid)
        if res:
            res = self.db.execute("DELETE FROM task WHERE userid = %s AND taskid = %s \
                AND status = 1")
            status = "true"
        else:
            status = "false"
        self.write_data({
                'status':status
            })

#/api/user/task
class TaskPerUserHandler(BaseHandler):
    @require_token
    def get(self):
        status = "false"
        userid = self.user['id']
        res = self.db.query("SELECT distinct(A.taskid),A.title,A.quote AS claimquote, B.quote,\
            DATE_FORMAT(B.date, '%%Y年%%m月%%d日') AS postdate ,\
            DATE_FORMAT(A.expiredate, '%%Y年%%m月%%d日') AS expiredate \
            FROM task A JOIN claim B ON \
            A.taskid = B.taskid WHERE B.userid = %s ORDER BY A.postdate DESC", userid)
        status = "true"
        self.write_data({
                'status':status,
                'data': res
            })