#coding=utf-8
import sys,smtplib,email,json
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart  


class mail():
    def __init__(self,configfile = "config/mail.cfg"): # Init config
        self.configfile = configfile
        cfg = json.load(file(self.configfile))
        self.pophost = cfg['pophost']
        self.smtphost = cfg['smtphost']
        self.mailfrom = cfg['username']
        self.password = cfg['password']
    # set subject
    def set_subject(self,subject = None):
        if isinstance(subject,unicode) is False:
	    try:
                subject = subject.decode('utf-8')
            except:
                subject = subject.decode('ascii')
        self.subject = subject
    # set mail body
    def set_body(self,body = None):
        sys.stderr.write("###############")
        sys.stderr.write(str(isinstance(body,unicode)))
        if isinstance(body,unicode) is False:
            try:
                sys.stderr.write("utf-8")
                body = body.decode('utf-8')
            except:
                sys.stderr.write("ascii")
                body = body.decode('ascii')
        self.body = body
    # set recipient and send mail
    def sendmail(self,recipient):
        self.recipient = recipient
        # set mail type
        send_mail_msg = MIMEMultipart()
        send_mail_msg['Subject'] = self.subject
        send_mail_msg['To'] = self.recipient
        send_mail_msg['From'] = self.mailfrom
        # set mail body
        Contents = MIMEText(self.body.encode('utf-8'),'html','utf-8')
        send_mail_msg.attach(Contents)
        # connect to smtp server
        smtp = smtplib.SMTP(self.smtphost)
        # login smtp
        smtp.login(self.mailfrom,self.password)
        # send mail
        smtp.sendmail(self.mailfrom, self.recipient, send_mail_msg.as_string())
        # quit server
        smtp.quit()
        print "Successfully."
        return

