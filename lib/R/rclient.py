import socket, time, json
import config, command
from .r import base as rbase

class rclient(rbase):
    def __init__(self):
        print "Below is rbase"
        rbase.__init__(self,\
            port=config.port,\
            host=config.host,\
            clientport=config.clientport,\
            clienthost=config.clienthost,\
            portinit=config.portinit,\
            portn=config.portn,\
            redisport=config.redisport,\
            redishost=config.redishost,\
            server=False)
    def run(self):
        print "R Client is running..."
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print self.clienthost
        sock.bind((self.clienthost, self.clientport))
        sock.listen(1)
        while True: 
            connection,address = sock.accept()  
            try:  
                connection.settimeout(5)  
                buf = json.loads(connection.recv(4096))
                token = buf['token']
                cmd = buf['cmd']
                rfile = buf['rfile']
                if not self.isrunning(token):
                    port = self.create(token,rfile)
                    print "python Rclient.py",token,port
                    time.sleep(1)
                    res = self.send(token,port,cmd)
                else:
                    port = self.getportbytoken(token)
                    res = self.send(token,port,cmd)
                    if len(res) != 0 and res != "NULL":
                        print res
                connection.send(res)
            except socket.timeout: 
                print 'time out'  
            connection.close() 
    def create(self,token,rfile):
        rfile = "R/"+str(rfile)
        if self.isrunning(token):
            return False
        else:
            if not self.checkport():
                return False
            else:
                client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                client_socket.connect((self.host, self.port))
                data = json.dumps({
                    'token':token,
                    'file':rfile
                    }).encode('utf-8')
                client_socket.sendall(data+"\n")     
                data = client_socket.recv(5000)
                print "Port",data
                client_socket.close()
                del client_socket
                return data
    def send(self,token,port,cmd):
        if not self.isrunning(token):
            return False
        else:
            self.update(token)
            cmd = command.command(cmd)
            cmd.filter()
            res = self.commandflood(token)
            print res
            if not res:
                return "You input too fast, please take a break."
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.connect(("localhost", int(port)))
            client_socket.sendall(cmd.getcmd()+"\n")     
            data = client_socket.recv(5000)
            client_socket.close()
            del client_socket
            return data
