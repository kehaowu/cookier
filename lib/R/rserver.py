import socket, time, json, psutil
import config
from os import fork as procfork
from os import execl as procexe
from os import _exit as procexit
from .r import base as rbase

class rserver (rbase):
    def __init__(self):
        rbase.__init__(self,\
            port=config.port,\
            host=config.host,\
            clientport=config.clientport,\
            clienthost=config.clienthost,\
            portinit=config.portinit,\
            portn=config.portn,\
            redisport=config.redisport,\
            redishost=config.redishost,\
            server=True)
        print self.port
        print self.host
    def run(self):
        print "Run at port %d" % (self.port)
        try:
            monitorpid = procfork()
            if monitorpid == 0:
                self.messager()
            else:
                self.cop()
        except KeyboardInterrupt: 
            print "User Press Ctrl+C,Exit" 
        except EOFError:  
            print "User Press Ctrl+D,Exit" 
    def messager(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((self.host, self.port))
        sock.listen(5) 
        while True: 
            connection,address = sock.accept()  
            try:  
                connection.settimeout(5)  
                buf = json.loads(connection.recv(4096))
                print "Welcome to server!" 
                token = buf['token']
                rfile = buf['file']
                if not self.isrunning(token):
                    if rfile == "kill":
                        connection.send('true')
                        self.rprockill(token)
                        continue
                    if not self.checkport():
                        connection.send(self.getportbytoken(token))
                        continue
                    port = self.randomport()
                    rserverpid = procfork()
                    if rserverpid == 0:
                        procexe("/usr/bin/Rscript","/usr/bin/Rscript", "R/server.R",port,rfile)
                        procexit(0)
                    rserverpid = str(rserverpid)
                    connection.send(str(port))
                    print token,port
                    self.rprocstart(token,port,rserverpid)
                    time.sleep(1)
            except socket.timeout: 
                print 'time out'  
            connection.close() 
    def cop(self):
        while True:
            self.checkalivepid()
            #Get all process id
            pids = self.getalivepid()
            #Check status of each process
            for pid in pids:
                p = psutil.Process(int(pid))
                elapse_time = (time.time() - p.create_time()) > 60
                memory_percent = p.memory_percent() > 3
                number_threads = p.num_threads() > 1
                status = (p.status() != "running" and p.status() != "sleeping")
                if elapse_time or memory_percent or number_threads or status:
                    print "Terminate process",pid
                    self.rprocstop(pid)
                    error_info = [
                        "timeout",
                        "exceed memory limitation",
                        "exceed threads limit",
                        "dead process"
                    ]
                    print "Due to",error_info[[elapse_time,memory_percent,number_threads,status].index(True)]

            #Check system CPU usage

            #Check system memory usage

            time.sleep(2)
