# R Daemon process manager
# R精灵进程管理器
## Redis 结构
集合：
>
(alive:port)			存储可用端口，默认端口为60020-60119
(dead:port)			存储占用端口
(alive:pid)			存储正在运行的进程ID
(dead:pid)			存储已经终止的进程ID

哈希表：
>
(token:pid)			存储token和对应的进程ID，随进程终止销毁
(token:port)			存储token和对应的端口号，随进程终止销毁

字符：
>
(pid+[pid]:port)		存储进程，设置过期时间

## rbase类的内置方法
### 1. 查询是否有空闲端口
checkport(port=None)

若port为空则查询是否有空闲端口，若不为空则查询指定端口是否被占用

### 2. 随机获取空闲端口
randomport()

### 3. 发起R精灵进程
rprocstart(token,port,pid,file,expire=600)

### 4. 终止R精灵进程
rprocstop(pid)

### 5. 判断token是否有对应进程在运行
isrunning(token)

### 6. 检查进程是否真实存在
isalivepid(pid)

### 7. 更新token过期时间
update(token,expire=600)

### 8. 获取进程是否过期
checkalivepid()

## rserver类
继承rbase类

## rclient类（虚拟）
继承rbase类