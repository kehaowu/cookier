from lib._pagebase import *
from markdown import markdown
import socket,json

class consoleHandler(BaseHandler):
    def get(self,contentid):
        try:
            opr = self.db.execute("UPDATE content SET viewcount = viewcount + 1 \
                WHERE contentid = %s", contentid)
            res = self.db.get("SELECT title,content,viewcount FROM content \
                WHERE contentid = %s", contentid)
            self.render("client/console.html",title = res['title'],viewcount = res['viewcount'])
        except:
            self.render("client/error/404.html")