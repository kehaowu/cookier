from lib._pagebase import BaseHandler
from markdown import markdown

class chapterListHandler(BaseHandler):
    def get(self,courseid):
        try:
            data = self.db.query("SELECT title FROM course \
                WHERE parentid = %s AND status = 1", courseid)
            content = ";".join([i['title'] for i in data])
            title = self.db.get("SELECT title FROM course\
                WHERE courseid = %s AND status = 1", courseid)['title']
            self.render("client/course.html",title=title, content = content.strip())
        except:
            self.render("client/error/404.html")

class indexHandler(BaseHandler):
    def get(self):
        try:
            self.render("client/index.html")
        except:
            self.render("client/error/404.html")

class passwdResetHandler(BaseHandler):
    def get(self,token):
        try:
            self.render("client/reset.html", token = token)
        except:
            self.render("client/error/404.html")

class forgetPasswdHandler(BaseHandler):
    def get(self):
        try:
            self.render("client/forget.html")
        except:
            self.render("client/error/404.html")

class taskHandler(BaseHandler):
    def get(self):
        try:
            self.render("client/task.html")
        except:
            self.render("client/error/404.html")

class pageNotFoundHandler(BaseHandler):
    def get(self):
        self.render("client/error/404.html")