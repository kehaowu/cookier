#!/usr/bin/env python
import time, datetime
from tornado import web, escape, httputil
from uuid import uuid1 as uuid
import md5,json

__all__ = [
    'BaseHandler',
    'require_token', 'option_page',
    'item_picker', 'PageMixIn',
]

httputil.responses.update({
    422: 'Unprocessable Entity',
})

def item_picker(keys):
    def pick(self, data):
        item = {}
        for i in keys:
            val = data.get(i)
            if val is not None:
                if isinstance(val, datetime.datetime):
                    val = val.isoformat()
                item[i] = val
        return item
    return pick

def option_page(func):
    def method(self, *k, **kw):
        try:
            self.page = int(self.get_param('page')) or 1
        except:
            self.page = 1
        func(self, *k, **kw)
    return method

class PageMixIn:
    per_page = 25
    def get_pages(self, total):
        pages = total // self.per_page
        if total % self.per_page: pages += 1
        return pages



class BaseHandler(web.RequestHandler):
    def get_param(self, key, default = None):
        value = self.params.get(key)
        return value if value is not None else default

    def write_data(self, data, status_code = None):
        if data is None and status_code is None:
            status_code = 204
        if status_code:
            self.set_status(status_code)
        if data is not None: self.write(data)
        raise web.Finish()

    def write_error_data(self, status_code, data = None, **kw):
        self.set_status(status_code)
        self.write_data({
            'code': status_code,
            'message': data or self._reason,
        })

    @property
    def db(self):
        return self.application.db

    def token(self):
        return md5.md5(str(uuid())).hexdigest()

    def encrypt(self,password):
        return md5.md5(password).hexdigest()

    def gettokenandrfile(self,userid,contentid):
        res = self.db.query("SELECT rfile,content FROM content \
            WHERE contentid = %s", contentid)
        token = userid+str(contentid)
        print res
        return {'token':token,'rfile':res[0]['rfile'],'content':res[0]['content']}

def require_token(func):
    def method(self, *args, **kwargs):
        if not hasattr(self, 'user'):
            token = self.request.headers.get('Authorization')
            if token:
                token = token.strip()
                if token.startswith('Token '):
                    token = token[6:]
                else:
                    token = None
            if token:
                res = self.db.get(
                    'SELECT user_id FROM token WHERE token=%s AND expire_time>CURRENT_TIMESTAMP',
                    token)
                if res:
                    user_id = res['user_id']
                    self.user = {
                        'id': user_id,
                        'token': token,
                    }
                    res = self.db.get('SELECT username FROM user WHERE id=%s', user_id)
                    self.user['username'] = res['username']
                else:
                    token = None
            if not token:
                self.write_error_data(401)
        func(self, *args, **kwargs)
    return method



def require_admintoken(func):
    def method(self, *args, **kwargs):
        if not hasattr(self, 'user'):
            token = self.request.headers.get('Authorization')
            if token:
                token = token.strip()
                if token.startswith('Token '):
                    token = token[6:]
                else:
                    token = None
            if token:
                res = self.db.get(
                    'SELECT user_id FROM admintoken WHERE token=%s AND expire_time>CURRENT_TIMESTAMP',
                    token)
                if res:
                    user_id = res['user_id']
                    self.user = {
                        'id': user_id,
                        'token': token,
                    }
                    res = self.db.get('SELECT username FROM admin WHERE id=%s', user_id)
                    self.user['username'] = res['username']
                else:
                    token = None
            if not token:
                self.write_error_data(401)
        func(self, *args, **kwargs)
    return method
