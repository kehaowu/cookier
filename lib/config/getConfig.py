#!/usr/bin/env python
import json

__all__ = ['MysqlConfig']

class MysqlConfig():
    '''class: MysqlConfig(dbID)
\tRead config/mysql.cfg file, return an instance contains unmodifieable property:
instance.all:\tall config in the config file
instance.host:\thost property of dbID
instance.user:\t user property of dbID
instance.passwd:\tpasswd property of dbID
instance.port:\tport property of dbID
instance.database:\tdatabase property of dbID
instance.charset:\tcharset property of dbID
instance.time_zone:\ttime_zone property of dbID
'''
    __slots__ = ('_all','_host','_user','_passwd','_port','_database','_charset','_time_zone')
    def __init__(self,dbID):
        cfgPath = 'config/mysql.cfg'
        self._all     = json.load(file(cfgPath))

        self._host    = self._all[dbID]['host']
        self._user    = self._all[dbID]['user']
        self._passwd  = self._all[dbID]['passwd']
        self._port    = self._all[dbID]['port']
        self._database= self._all[dbID]['database']
        self._charset = self._all[dbID]['charset']
        self._time_zone=self._all[dbID]['time_zone']

    @property
    def all(self):
        return self._all

    @property
    def host(self):
        return self._host

    @property
    def user(self):
        return self._user

    @property
    def passwd(self):
        return self._passwd

    @property
    def port(self):
        return self._port

    @property
    def database(self):
        return self._database

    @property
    def charset(self):
        return self._charset

    @property
    def time_zone(self):
        return self._time_zone
    
    
if __name__ == '__main__':
    print '''class: MysqlConfig(dbID)
\tRead config/mysql.cfg file, return an instance contains unmodifieable property:
instance.all:\tall config in the config file
instance.host:\thost property of dbID
instance.user:\t user property of dbID
instance.passwd:\tpasswd property of dbID
instance.port:\tport property of dbID
instance.database:\tdatabase property of dbID
instance.charset:\tcharset property of dbID
instance.time_zone:\ttime_zone property of dbID
'''
