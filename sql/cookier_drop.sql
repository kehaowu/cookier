-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2015-07-29 19:43:15.055


DROP TABLE IF EXISTS admin;
DROP TABLE IF EXISTS admincourse;
DROP TABLE IF EXISTS admintoken;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS token;
DROP TABLE IF EXISTS record;
DROP TABLE IF EXISTS course;
DROP TABLE IF EXISTS content;
DROP TABLE IF EXISTS rechargecard;
DROP TABLE IF EXISTS rechargecardprivilege;
DROP TABLE IF EXISTS passwdreset;
DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS claim;

DROP TRIGGER IF EXISTS adminexpire;
DROP TRIGGER IF EXISTS expire;
DROP TRIGGER IF EXISTS rechargecardexpire;
DROP TRIGGER IF EXISTS passwdresetexpire;

-- End of file.

