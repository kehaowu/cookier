DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS claim;

ALTER TABLE content ADD good INT NOT NULL DEFAULT 0;
ALTER TABLE content ADD bad INT NOT NULL DEFAULT 0;

CREATE TABLE evaluation (
    id INT NOT NULL AUTO_INCREMENT,
    userid INT NOT NULL,
    contentid VARCHAR(32) NOT NULL,
    good INT NOT NULL DEFAULT 0,
    bad INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);