ALTER DATABASE cookier CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci';

CREATE TABLE admin (
    id int NOT NULL AUTO_INCREMENT,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    phone varchar(255) NULL,
    status int NOT NULL DEFAULT 1 ,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE KEY username (username),
    UNIQUE KEY email (email)
);

CREATE TABLE admincourse (
    id int NOT NULL AUTO_INCREMENT,
    adminid int NOT NULL,
    courseid int NOT NULL,
    status int NOT NULL DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE admintoken (
    token varchar(255) NOT NULL,
    user_id int NOT NULL,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    expire_time TIMESTAMP,
    PRIMARY KEY (token)
);

CREATE TRIGGER adminexpire BEFORE INSERT ON admintoken
FOR EACH ROW SET NEW.expire_time = CURRENT_TIMESTAMP + INTERVAL 7 DAY;

CREATE TABLE user (
    id int NOT NULL AUTO_INCREMENT,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    phone varchar(255) NULL,
    credit int NOT NULL DEFAULT 50,
    status int NOT NULL DEFAULT 1 ,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE KEY username (username),
    UNIQUE KEY email (email)
);

CREATE TABLE token (
    token varchar(255) NOT NULL,
    user_id int NOT NULL,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    expire_time TIMESTAMP,
    PRIMARY KEY (token)
);

CREATE TRIGGER expire BEFORE INSERT ON token
FOR EACH ROW SET NEW.expire_time = CURRENT_TIMESTAMP + INTERVAL 30 DAY;

CREATE TABLE record (
    id int NOT NULL AUTO_INCREMENT,
    userid int NOT NULL,
    contentid varchar(32) NOT NULL,
    updatetime DATETIME NOT NULL,
    createtime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status int NOT NULL DEFAULT 0,
    CONSTRAINT record_pk PRIMARY KEY (id)
);

-- course and chapter
-- course parentid = 0
CREATE TABLE course (
    id INT NOT NULL AUTO_INCREMENT,
    courseid INT NOT NULL,
    parentid INT NOT NULL DEFAULT 0,
    title TEXT NOT NULL,
    description TEXT NULL,
    createtime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatetime DATETIME NOT NULL,
    weight INT NOT NULL DEFAULT 0,
    status INT NOT NULL DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE content (
    id INT NOT NULL AUTO_INCREMENT,
    contentid VARCHAR(32) NOT NULL,
    chapterid INT NOT NULL,
    title text NOT NULL,
    rfile VARCHAR(100) NOT NULL,
    content TEXT NOT NULL,
    createtime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatetime DATETIME NOT NULL,
    credit INT NOT NULL DEFAULT 0,
    viewcount INT NOT NULL DEFAULT 0,
    good INT NOT NULL DEFAULT 0,
    bad INT NOT NULL DEFAULT 0,
    weight INT NOT NULL DEFAULT 0,
    status INT NOT NULL DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE evaluation (
    id INT NOT NULL AUTO_INCREMENT,
    userid INT NOT NULL,
    contentid VARCHAR(32) NOT NULL,
    good INT NOT NULL DEFAULT 0,
    bad INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE rechargecard (
    id INT NOT NULL AUTO_INCREMENT,
    cardid VARCHAR(255) NOT NULL,
    credit INT NOT NULL DEFAULT 100,
    createtime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    rechargetime DATETIME NULL,
    userid INT NULL,
    expire_time TIMESTAMP NOT NULL,
    adminid INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TRIGGER rechargecardexpire BEFORE INSERT ON rechargecard
FOR EACH ROW SET NEW.expire_time = CURRENT_TIMESTAMP + INTERVAL 14 DAY;

CREATE TABLE rechargecardprivilege (
    adminid INT NOT NULL,
    status INT NOT NULL DEFAULT 1
);

CREATE TABLE passwdreset (
    id INT NOT NULL AUTO_INCREMENT,
    email varchar(255) NOT NULL,
    token VARCHAR(255) NOT NULL,
    expiretime TIMESTAMP NULL,
    status INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TRIGGER passwdresetexpire BEFORE INSERT ON passwdreset 
FOR EACH ROW SET NEW.expiretime = now() + INTERVAL 2 HOUR;

CREATE TABLE task (
    taskid INT NOT NULL AUTO_INCREMENT,
    userid INT NOT NULL,
    title VARCHAR(255) NOT NULL,
    quote INT NOT NULL DEFAULT 0,
    content TEXT NOT NULL,
    postdate DATETIME NOT NULL,
    editdate DATETIME NOT NULL,
    expiredate DATETIME NOT NULL,
    status INT NOT NULL DEFAULT 1,
    PRIMARY KEY (taskid)
);

CREATE TABLE claim (
    id INT NOT NULL AUTO_INCREMENT,
    taskid INT NOT NULL,
    userid INT NOT NULL,
    quote INT NOT NULL,
    date DATETIME NOT NULL,
    status INT NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);
