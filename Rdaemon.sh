#!/bin/bash

RDaemonCore="RDaemonCore.py"
RDaemon="RDaemon.py"

function init(){
    echo "Init ..."
    pkill -9 -f RDaemon.py
    pkill -9 -f RDaemonCore.py
    pkill -9 -f server.R
    sleep 1
}

function restart(){
    if [ $# -eq 1 ]; then
        debug="T"
    else
        debug="F"
    fi
    debug="T"
    pkill -9 -f RDaemon.py
    pkill -9 -f RDaemonCore.py
    pkill -9 -f server.R
    sleep 1
    netstat -ltap |grep -E "60010|60011"
    if [ $debug = "T" ]; then
        echo "Restarting Rmanager with debug mode... ..."
        python $RDaemonCore &
        echo "Restarting Rclient with debug mode... ..."
        python $RDaemon &
    else
        echo "Restarting Rmanager ... ..."
        python $RDaemonCore >>RDaemonCore.log 2>&1 &
        echo "Restarting Rclient ... ..."
        python $RDaemon >>RDaemon.log 2>&1 &
    fi
}

function Rdaemon(){
    pid=`ps -ef|grep $1|grep -v grep|wc -l`
    if [ $pid -eq 0 ]; then
        echo $1 $pid
        restart
        sleep 1
    fi
}

init
while [ 1 == 1 ]
do
    Rdaemon "RDaemonCore.py" 
    Rdaemon "RDaemon.py" 
    sleep 1
done
