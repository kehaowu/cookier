from lib._pagebase import BaseHandler

class CourseHandler(BaseHandler):
    def get(self,courseid):
        self.render("admin/course.html")

class IndexHandler(BaseHandler):
    def get(self):
        self.render("admin/index.html")

class rechargeHandler(BaseHandler):
    def get(self):
        self.render("admin/recharge.html")

