#!/usr/bin/env python
# coding=utf-8
from lib._base import *
import uuid, time, hashlib, random, base64

class TokenMixIn:
    def generate_token(self, user_id):
        rand = hashlib.md5((
            str(time.time()) +
            str(random.uniform(0,1)) +
            str(user_id).zfill(8)
        ).encode()).digest()
        token = base64.b64encode(uuid.uuid4().bytes + rand[:64]).decode()
        self.db.execute(
            'INSERT INTO admintoken (user_id,token) VALUES(%s,%s)',
            user_id, token)
        return token

class SignUpHandler(TokenMixIn, BaseHandler):
    def post(self):
        nickname = "A"
        username = self.get_param('username')
        token = self.get_param('token')
        token_ = "iwmsasd12asd1dsa"
        if not username or token != token_ :
            self.write_error_data(422, {
                'code': 100,
                'message': 'Username is required.',
            })
        exists = self.db.query('SELECT username FROM admin WHERE username=%s', username)
        if exists:
            self.write_error_data(422, {
                'code': 101,
                'message': 'Username exists.',
            })
        email = self.get_param('email')
        if email.find('@') < 0:
            self.write_error_data(422, {
                'code': 200,
                'message': 'Invalid email.',
            })
        exists = self.db.query('SELECT email FROM admin WHERE email = %s', email)
        if exists:
            self.write_error_data(422, {
                'code': 201,
                'message': 'Email has been signed up.',
            })
        pwd = self.get_param('password')
        if not pwd or len(pwd) < 6:
            self.write_error_data(422, {
                'code': 300,
                'message': 'Invalid password.',
            })
        try:
            user_id = self.db.execute(
                'INSERT INTO admin (username,password,email) '
                'VALUES (%s,PASSWORD(%s),%s)',
                username,pwd,email)
            assert user_id
        except:
            self.write_error_data(500)
        token = self.generate_token(user_id)
        self.write_data({
            'id': user_id,
            'token': token,
            'username': username,
        })

class SignInHandler(TokenMixIn, BaseHandler):
    def post(self):
        username = self.get_param('username')
        password = self.get_param('password')
        email = None
        if not username or not password or len(password) < 6:
            self.write_error_data(422, 'Invalid username or password.')
        if username.find('@') > 0:
            username, email = None, username
        sql = ['SELECT id,username FROM admin WHERE status=1 AND password=PASSWORD(%s)']
        args = [password]
        if username:
            sql.append('username=%s')
            args.append(username)
        elif email:
            sql.append('email=%s')
            args.append(email)
        res = self.db.get(' AND '.join(sql), *args)
        if not res:
            self.write_error_data(401, 'Invalid username or password.')
        user_id = res['id']
        username = res['username']
        token = self.generate_token(user_id)
        self.write_data({
            'id': user_id,
            'token': token,
            'username': username,
        })

class UserInfoHandler(BaseHandler):
    @require_token
    def get(self):
        self.write_data(self.user)

class LogOutHandler(BaseHandler):
    @require_token
    def delete(self):
        self.db.execute('DELETE FROM admintoken WHERE token=%s', self.user['token'])
        self.write_data(None)

