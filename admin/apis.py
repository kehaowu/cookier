# encoding=utf-8
from lib._base import *
import re
import uuid, time, hashlib, random, base64


#/admin/api/course 
#GET ALL course; POST Create new course
class courseHandler(BaseHandler):
    @require_admintoken
    def get(self):
        try:
            info = self.db.query("SELECT A.courseid,A.title,A.description FROM course A JOIN admincourse B \
                ON A.courseid = B.courseid WHERE A.parentid = 0 \
                AND A.status = 1 \
                AND B.adminid = %s", self.user['id'])
            status = "true"
        except:
            info = []
            status = "false"
        print info
        self.write_data({
                'status':status,
                'data':info
            })
    @require_admintoken
    def post(self):
        data = self.jsonload(self.get_argument("data"))
        try:
            newcourseid = self.db.query("SELECT max(courseid) FROM course;")
            newcourseid = newcourseid[0]['max(courseid)']
            if newcourseid == None:
                newcourseid = 0
            newcourseid = newcourseid + 1
            self.db.execute("INSERT INTO course \
                (courseid,parentid,title,description,createtime,updatetime) VALUES \
                (%s,0,%s,'',now(),now())",newcourseid,data['title'])
            self.db.execute("INSERT INTO admincourse (courseid,adminid) \
                VALUES (%s,%s)",newcourseid,self.user['id'])
            if self.user['id'] != 1:
                self.db.execute("INSERT INTO admincourse (courseid,adminid) \
                    VALUES (%s,1)",newcourseid)
            status = "true"
        except Exception,e:
            print e
            newcourseid = 0
            status = "false"
        self.write_data({
                        'status':status,
                        'data':{
                            'courseid':newcourseid
                        }
                    })

#/admin/api/course/(\w+) 
#GET Details; POST Modify; DELETE Delete
class courseByidHandler(BaseHandler):
    @require_admintoken
    def get(self,courseid):
        try:
            info = self.db.query("SELECT A.courseid,A.title,A.description FROM course A JOIN admincourse B ON \
                A.courseid = B.courseid WHERE A.status = 1 \
                AND A.courseid = %s AND A.parentid = 0 AND B.adminid = %s", courseid,self.user['id'])
            status = "true"
        except:
            info = ""
            status = "false"
        self.write_data({
                        'status':status,
                        'data':info
                    })
    @require_admintoken
    def post(self,courseid):
        try:
            data = self.jsonload(self.get_argument("data"))
            res = self.db.query("SELECT A.courseid,A.title,A.description FROM course A JOIN admincourse B ON \
                A.courseid = B.courseid WHERE A.status = 1 \
                AND A.courseid = %s AND A.parentid = 0 AND B.adminid = %s", courseid,self.user['id'])
            if res != None:
                info = self.db.execute("UPDATE course SET \
                    title = %s,\
                    description = %s \
                    WHERE courseid = %s AND parentid = 0",\
                    data['title'].encode("utf-8"),data['description'].encode("utf-8"),courseid)
                print "OK"
                print info
                status = "true"
            else:
                status = "none"
        except Exception,e:
            print e
            status = str(e)
        self.write_data({
                        'status':status
                    })
    @require_admintoken
    def delete(self,courseid):
        try:
            res = self.db.execute("UPDATE course SET status = 0 \
                WHERE courseid = %s AND parentid = 0",courseid)
            status = "true"
        except Exception,e:
            print e
            status = "false"
        self.write_data({
                        'status':status,
                    })

# /admin/api/course/(\w+)/chapter 
# GET Chapter list; POST Create new chapter
class chapterByCourseidHandler(BaseHandler):
    @require_admintoken
    def get(self,courseid):
        info = ""
        try:
            res = self.db.query("SELECT courseid FROM course\
                WHERE courseid = %s AND status = 1", courseid)
            print res[0]['courseid']
            if res[0]['courseid'] != None:
                info = self.db.query("SELECT courseid,title FROM course\
                    WHERE parentid = %s AND status = 1",courseid)
                status = "true"
            else:
                status = "false"
        except Exception,e:
            print e
            status = "false"
        self.write_data({
                        'data':info,
                        'status':status
                    })
    @require_admintoken
    def post(self,courseid):
        data = self.jsonload(self.get_argument("data"))
        res = self.db.query("SELECT courseid FROM course \
            WHERE parentid = 0 AND status = 1")
        if res[0]['courseid'] == None:
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            newchapterid = self.db.query("SELECT max(courseid) FROM course;")
            newchapterid = newchapterid[0]['max(courseid)']
            if newchapterid == None:
                newchapterid = 0
            newchapterid = newchapterid + 1
            res = self.db.execute("INSERT INTO course \
                (courseid,parentid,title,description,createtime,updatetime) VALUES \
                (%s,%s,%s,'',now(),now())",newchapterid,courseid,data['title'])
            status = "true"
        except Exception,e:
            print e
            newchapterid = 0
            status = "false"
        self.write_data({
                        'status':status,
                        'data':{
                            'chapterid':newchapterid
                        }
                    })

# /admin/api/chapter/(\w+)
# GET Details; POST Modify; DELETE Delete
class chapterByidHandler(BaseHandler):
    @require_admintoken
    def get(self,chapterid):
        info = ""
        res = self.db.query("SELECT courseid FROM course \
            WHERE parentid != 0 AND status = 1")  
        if res[0]['courseid'] == None:
            print "HERE"
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            info = self.db.query("SELECT courseid,title,description FROM course \
                WHERE courseid = %s AND status = 1",chapterid)
            status = "true"
        except Exception,e:
            status = "false"
            print e
        self.write_data({
                        'status':status,
                        'data':info
                    })
    @require_admintoken
    def post(self,chapterid):
        res = self.db.query("SELECT courseid FROM course \
            WHERE parentid != 0 AND status = 1")  
        if res[0]['courseid'] == None:
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            data = self.jsonload(self.get_argument("data"))
            res = self.db.execute("UPDATE course SET \
                title = %s, description = %s WHERE \
                courseid = %s AND status = 1",\
                data['title'],data['description'],chapterid)
            status = "true"
        except Exception,e:
            print e
            status = "false"
        self.write_data({
                        'status':status,
                        'data':{
                            'chapterid' : chapterid
                        }
                    })
    @require_admintoken
    def delete(self,chapterid):
        res = self.db.query("SELECT courseid FROM course \
            WHERE parentid != 0 AND status = 1")  
        if res[0]['courseid'] == None:
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            self.db.execute("UPDATE course SET status = 0\
                WHERE courseid = %s", chapterid)
            status = "true"
        except Exception,e:
            print e
            status = "false"
        self.write_data({
                        'status':status,
                        'data':{
                            'chapterid' : chapterid
                        }
                    })


#/admin/api/chapter/(\w+)/content
# GET Content list; POST Create new content
class contentByChapteridHandler(BaseHandler):
    @require_admintoken
    def get(self,chapterid):
        res = self.db.query("SELECT courseid FROM course \
            WHERE parentid != 0 AND status = 1")  
        if res == None:
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            info = self.db.query("SELECT contentid,title FROM content \
                WHERE chapterid = %s AND status = 1 ORDER BY weight", chapterid)
            status = 'true'
        except Exception,e:
            status = 'false'
            print e
        self.write_data({
                        'status':status,
                        'data':info
                    })
    @require_admintoken
    def post(self,chapterid):
        res = self.db.query("SELECT courseid FROM course \
            WHERE parentid != 0 AND status = 1 AND courseid = %s",\
            chapterid)
        print res
        if len(res) == 0:
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            data = self.jsonload(self.get_argument("data"))
            data['rfile'] = "test.RData"
            contentid = self.token()
            self.db.execute("INSERT INTO content \
                (contentid,chapterid,title,rfile,content,createtime,updatetime)\
                VALUES(%s,%s,%s,%s,%s,now(),now())",\
                contentid,chapterid,data['title'],data['rfile'],data['content'])
            status = "true"
        except Exception,e:
            print e
            status = "fasle"
        self.write_data({
                        'status':status,
                        'data':{
                            'contentid':contentid
                        }
                    })

# /admin/api/content/(\w+)
# GET Details; POST Modify; DELETE Delete
class contentByidHandler(BaseHandler):
    @require_admintoken
    def get(self,contentid):
        res = self.db.query("SELECT contentid FROM content \
            WHERE status = 1")  
        if res[0]['contentid'] == None:
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            info = self.db.query("SELECT contentid,chapterid,title,rfile,content,weight \
                FROM content WHERE status = 1 AND contentid = %s", \
                contentid)
            status = "true"
        except Exception,e:
            status = "false"
            print e
        self.write_data({
                        'status':status,
                        'data':info
                    })
    @require_admintoken
    def post(self,contentid):
        res = self.db.query("SELECT contentid FROM content \
            WHERE status = 1 AND contentid = %s",contentid)
        if res[0]['contentid'] == None:
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            data = self.jsonload(self.get_argument("data"))
            data['rfile'] = "test.RData"
            info = self.db.execute("UPDATE content SET \
                title = %s, rfile = %s, content = %s, updatetime = now(), weight = %s\
                WHERE contentid = %s",
                data['title'],data['rfile'],data['content'],data['weight'],contentid)
            status = "true"
        except Exception,e:
            status = "false"
        self.write_data({
                        'status':status,
                        'data':{
                            'contentid':contentid
                        }
                    })
    @require_admintoken
    def delete(self,contentid):
        res = self.db.query("SELECT contentid FROM content \
            WHERE status = 1")  
        if res[0]['contentid'] == None:
            self.write_data({
                            'status':'fasle'
                        })
            return 
        try:
            res = self.db.execute("UPDATE content SET status = 0 WHERE \
                contentid = %s", contentid)
            status = "true"
        except Exception,e:
            status = "false"
            print e
        self.write_data({
                        'status':status,
                        'data':{
                            'contentid':contentid
                        }
                    })

# /admin/api/recharge/
# GET list; POST Modify;
class rechargeHandler(BaseHandler):
    @require_admintoken
    def get(self):
        res = []
        query  = self.db.get("SELECT adminid FROM \
            rechargecardprivilege WHERE adminid = %s \
            AND status = 1",self.user['id'])
        print query
        if query:
            try:
                res0 = self.db.query("SELECT cardid,credit,createtime,\
                    rechargetime, userid, expire_time, adminid \
                    FROM rechargecard")
                res = []
                for item in res0:
                    print "S"
                    cardid = item.cardid
                    credit = item.credit
                    adminid = item.adminid
                    createtime = item.createtime.strftime("%Y-%m-%d")
                    expire_time = item.expire_time.strftime("%Y-%m-%d")
                    if item.rechargetime != None:
                        rechargetime = item.rechargetime.strftime("%Y-%m-%d")
                    else:
                        rechargetime = ""
                    if item.userid == None:
                        userid = ""
                    else:
                        userid = item.userid
                    res.append({
                        'cardid':cardid,
                        'credit':credit,
                        'createtime':createtime,
                        'rechargetime':rechargetime,
                        'userid':userid,
                        'expire_time':expire_time,
                        'adminid':adminid
                        })
                print res
                status = "true"
            except Exception,e:
                print e
                status = "false"
        else:
            status = "false"
        self.write_data({
                        'status':status,
                        'data':res
                    })
    @require_admintoken
    def post(self):
        query  = self.db.get("SELECT adminid FROM \
            rechargecardprivilege WHERE adminid = %s \
            AND status = 1",self.user['id'])
        print query
        if query:
            try:
                rand = hashlib.md5((
                    str(time.time()) +
                    str(random.uniform(0,1)) +
                    str(random.uniform(0,1)).zfill(8)
                ).encode()).digest()
                cardid = str(base64.b64encode(uuid.uuid4().bytes + rand[:64]).decode())
                data = self.jsonload(self.get_argument("data"))
                if data['credit'] :
                    print "A"
                    query = self.db.execute("INSERT INTO rechargecard\
                        (cardid,credit,createtime,adminid) \
                        VALUES (%s,%s,now(),%s)",cardid,data['credit'],self.user['id'])
                    print "B"
                    status = "true"
                else:
                    status = "false"
            except Exception,e:
                print e
                status = "false"
        else:
            status = "false"
        self.write_data({
                        'status':status,
                        'data':{
                            'cardid':cardid
                        }
                    })
